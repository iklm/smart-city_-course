package com.example.smartcity.ui.notifications.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.smartcity.R
import com.example.smartcity.databinding.ActivitySmartBusOrderListBinding
import com.example.smartcity.ui.notifications.fragment.SmartBusOrderListFragment
import com.example.smartcity.ui.notifications.fragment.SmartBusOrderListFragment.Companion.IfOrderNumber
import com.google.android.material.tabs.TabLayoutMediator

/**
 * 智慧巴士 - 订单列表
 *
 */
class SmartBusOrderListActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivitySmartBusOrderListBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivitySmartBusOrderListBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        // 显示标题
        title = "订单列表"
        // 显示回退按钮
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // 配置TabLayout / ViewPager2
        setTabLayoutViewPager()
    }


    /**
     * 配置TabLayout / ViewPager2
     */
    private fun setTabLayoutViewPager() {

        // ViewPager2
        mBinding.smbsOrderListViewPager2.adapter = object : FragmentStateAdapter(this) {
            override fun getItemCount(): Int = 2

            override fun createFragment(position: Int): Fragment {
                return when (position) {
                    0 -> SmartBusOrderListFragment()
                    1 -> SmartBusOrderListFragment()
                    else -> SmartBusOrderListFragment()
                }
            }

            /**
             * @param IfOrderNumber 订单状态
             *
             * IfOrderNumber
             * @see com.example.smartcity.ui.notifications.fragment.SmartBusOrderListFragment
             *
             * position是根据TabLayout的滑动标签来获取下标值， 未支付是0， 已支付是1，
             * 我们根据下标来赋值并返回数据， 然后展示列表那边就可以根据订单状态数据来获取不同的数据
             *
             */
            override fun getItemId(position: Int): Long {
                IfOrderNumber = when (position) {
                    0 -> 0
                    1 -> 1
                    else -> 0
                }
                return super.getItemId(position)
            }
        }


        // 配置TabLayout
        TabLayoutMediator(mBinding.smbsOrderListTabLayout, mBinding.smbsOrderListViewPager2)
        { tab, position ->
            when(position) {
                0 -> tab.text = "未支付"
                1 -> tab.text = "已支付"
            }
        }.attach()

    }


    /**
     *  监听回退按钮事件
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // 销毁该界面
        finish()
        return super.onOptionsItemSelected(item)
    }
}