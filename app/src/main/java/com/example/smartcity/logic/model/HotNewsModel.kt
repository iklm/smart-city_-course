package com.example.smartcity.logic.model

import com.google.gson.annotations.SerializedName




/**
 *
 *  ANDROID STUDIO version 2022.1
 *  codeAuthor: ikun
 *  email/qq: 2506789532@qq.com
 *  createTime: 2023/5/7 22:29
 *  ------------------------------
 *
 **/
class HotNewsModel {
    @SerializedName("code")
    var code: Int? = null

    @SerializedName("msg")
    var msg: String? = null

    @SerializedName("rows")
    var rows: List<RowsDTO>? = null

    @SerializedName("total")
    var total: Int? = null

    class RowsDTO {
        @SerializedName("id")
        var id: Int? = null

        @SerializedName("cover")
        var cover: String? = null

        @SerializedName("title")
        var title: String? = null

        @SerializedName("subTitle")
        var subTitle: String? = null

        @SerializedName("content")
        var content: String? = null

        @SerializedName("status")
        var status: String? = null

        @SerializedName("publishDate")
        var publishDate: String? = null

        @SerializedName("tags")
        var tags: Any? = null

        @SerializedName("commentNum")
        var commentNum: Int? = null

        @SerializedName("likeNum")
        var likeNum: Int? = null

        @SerializedName("readNum")
        var readNum: Int? = null

        @SerializedName("type")
        var type: String? = null

        @SerializedName("top")
        var top: String? = null

        @SerializedName("hot")
        var hot: String? = null
    }
}