package com.example.smartcity.ui.SmartCity.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.smartcity.databinding.RChinabuildOneItemBinding
import com.example.smartcity.logic.base.BaseRecyclerAdapter
import com.example.smartcity.ui.SmartCity.Activity.mOneShowActivity
import com.example.smartcity.ui.SmartCity.model.mOneMoodel

/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/9/15 23:14
 *  created: Ikun
 *  Email: 2506789532@qq.com
 *
 *
 *  中国智造-厂商招聘列表适配器
 **/
class mOneRvAdapter(
    // 数据源
    private val list: MutableList<mOneMoodel>,
    // 布局Id
    private val layoutId: Int
) : BaseRecyclerAdapter<mOneMoodel, RChinabuildOneItemBinding>(list, layoutId){
    override fun setData(
        mBinding: RChinabuildOneItemBinding,
        data: mOneMoodel,
        position: Int,
        holder: ViewHolder
    ) {
        list[position].apply {
            // 填充数据
            mBinding.TvOnePost.text = this.tvTitle // 招聘标题
            mBinding.TvOneMoney.text = this.tvMoney // 招聘薪资
            mBinding.TvOneContent.text = this.tvContent // 招聘内容
        }.apply {
            val tvData = this
            // 点击进入查看详情页面
            holder.itemView.apply {
                this.setOnClickListener {
                    this.context.apply {
                        this.startActivity(Intent(this,
                            mOneShowActivity::class.java).apply {
                            // 传入标题内容
                            this.putExtra("title", tvData.tvTitle)
                            // 传入薪资
                            this.putExtra("money", tvData.tvMoney)
                            // 传入招聘内容
                            this.putExtra("content", tvData.tvContent)
                        })
                    }
                }
            }
        }
    }

    /**
     * 绑定视图
     */
    override fun getInflate(
        layoutInflater: LayoutInflater,
        parent: ViewGroup
    ): RChinabuildOneItemBinding {
        return RChinabuildOneItemBinding.inflate(layoutInflater, parent, false)
    }

}