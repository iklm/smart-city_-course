package com.example.smartcity.ui.SmartCity.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import com.example.smartcity.R
import com.example.smartcity.databinding.ActivityMoneShowBinding


/**
 * 中国智造-厂商招聘列表详情
 */
class mOneShowActivity : AppCompatActivity() {

    lateinit var mBinding: ActivityMoneShowBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityMoneShowBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        // 显示回退按钮
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        // 设置标题
        title = "招聘详情"

        intent.apply {
            var title = this.getStringExtra("title")
            var money = this.getStringExtra("money")
            var content = this.getStringExtra("content")

            // 输出日志看看
//            Log.d("mOneShowActivity", "onCreate: title -> $title")
//            Log.d("mOneShowActivity", "onCreate: money -> $money")
//            Log.d("mOneShowActivity", "onCreate: content -> $content")

            // 将数据填充到UI上
            mBinding.mShowTitle.text = title
            mBinding.mShowMoney.text = money
            mBinding.mShowContent.text = content
        }

    }

    /**
     * 点击回退按钮时销毁该界面
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}