package com.example.smartcity.ui.AllService.SmartBus.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import com.example.smartcity.R
import com.example.smartcity.databinding.ActivitySmartBusDateBinding
import com.example.smartcity.logic.Repository
import com.example.smartcity.logic.util.Util.show


/**
 * 智慧巴士 - 第二页面 - 日历日期选择
 */
class SmartBusDateActivity : AppCompatActivity() {

    private val TAG = SmartBusDateActivity::class.java.simpleName

    // 接收路线ID
    private val lineID by lazy { intent.getStringExtra("id") }

    private lateinit var mBinding: ActivitySmartBusDateBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivitySmartBusDateBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        // 设置标题
        title = "出行日期"
        // 显示回退按钮
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // 把当前意图添加到数组中
        Repository.smartAddActivity(this)

        // 输出日志
        Log.d(TAG, "onCreate: 路线ID --> $lineID")

        onSelectDate()
    }


    /**
     * 选择日期功能
     */
    private fun onSelectDate() {
        // 按钮状态 (只有选择好日期才能下一步)
        var ff = false

        // 存在选择的日期
        var strTime = ""

        // 监听日历
        mBinding.smbsDateCalendar.setOnDateChangeListener { view, nain, yue, ri ->
            runOnUiThread {
                // 监听是否选择日期， 选择了我们就填充到TextView上
                mBinding.smbsDateTitle.text = "您选择的出行日期: ${nain}年 ${yue}月 ${ri}日"
                // 赋值给我们创建的字符串变量
                strTime = "${nain}年 ${yue}月 ${ri}日"
                // 如果选择了日期就可以点击按钮
                ff = true
            }
        }

        // 点击跳转 (根据状态来判断按钮是否可以点击)
        mBinding.smbsDateBtn.setOnClickListener {
            if (ff) {
                // 跳转
                startActivity(Intent(this, SmartBusPCActivity::class.java).apply {
                    // 传入路线id
                    putExtra("lineID", lineID.toString())
                    // 传入我们已经选择的日期信息
                    putExtra("date", strTime)
                })
            } else "您还没选择出行日期!".show()
        }

    }

    /**
     * 点击回退按钮时销毁该页面
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}