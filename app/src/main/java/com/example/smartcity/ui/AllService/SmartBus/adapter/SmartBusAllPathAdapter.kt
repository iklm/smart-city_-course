package com.example.smartcity.ui.AllService.SmartBus.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.smartcity.databinding.ReBusHomeAllBinding
import com.example.smartcity.logic.base.BaseRecyclerAdapter
import com.example.smartcity.ui.AllService.SmartBus.model.SmartBusAllPathModel

/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/9/22 14:58
 *  created: Ikun
 *  Email: 2506789532@qq.com
 *
 *  智慧巴士 - 更多路线适配器
 *  @see com.example.smartcity.ui.AllService.SmartBus.SmartBusActivity
 *
 *  实体类
 *  @see com.example.smartcity.ui.AllService.SmartBus.model.SmartBusAllPathModel
 */
class SmartBusAllPathAdapter(
    // 数据源
    private val list: MutableList<SmartBusAllPathModel>, // 数据源列表
    private val layoutId: Int // 布局文件资源ID
) : BaseRecyclerAdapter<SmartBusAllPathModel, ReBusHomeAllBinding>(list, layoutId) {

    /**
     * 填充数据到UI
     *
     * @param mBinding 数据绑定对象
     * @param data 数据对象
     * @param position 数据在列表中的位置
     * @param holder ViewHolder
     */
    override fun setData(
        mBinding: ReBusHomeAllBinding,
        data: SmartBusAllPathModel,
        position: Int,
        holder: ViewHolder
    ) {
        // 获取数据填充到UI上
        list[position].apply {
            mBinding.tvBusAll.text = this.title // 将title属性填充到tvBusAll控件中
        }
    }

    /**
     * 创建并返回数据绑定对象
     *
     * @param layoutInflater 布局加载器
     * @param parent 父ViewGroup
     * @return ReBusHomeAllBinding 返回数据绑定对象
     */
    override fun getInflate(
        layoutInflater: LayoutInflater,
        parent: ViewGroup
    ): ReBusHomeAllBinding {
        return ReBusHomeAllBinding.inflate(layoutInflater, parent, false) // 使用布局加载器加载ReBusHomeAllBinding对象
    }

}