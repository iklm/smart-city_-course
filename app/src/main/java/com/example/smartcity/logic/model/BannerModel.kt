package com.example.smartcity.logic.model

import com.google.gson.annotations.SerializedName




/**
 *
 *  ANDROID STUDIO version 2022.1
 *  codeAuthor: ikun
 *  email/qq: 2506789532@qq.com
 *  createTime: 2023/5/6 23:01
 *  ------------------------------
 *
 **/
class BannerModel {
    @SerializedName("total")
     val total: Int? = null

    @SerializedName("rows")
     val rows: List<RowsDTO>? = null

    @SerializedName("code")
     val code: Int? = null

    @SerializedName("msg")
     val msg: String? = null

    class RowsDTO {
        @SerializedName("searchValue")
         val searchValue: Any? = null

        @SerializedName("createBy")
         val createBy: String? = null

        @SerializedName("createTime")
         val createTime: String? = null

        @SerializedName("updateBy")
         val updateBy: String? = null

        @SerializedName("updateTime")
         val updateTime: String? = null

        @SerializedName("remark")
         val remark: Any? = null

        @SerializedName("params")
         val params: ParamsDTO? = null

        @SerializedName("id")
         val id: Int? = null

        @SerializedName("appType")
         val appType: String? = null

        @SerializedName("status")
         val status: String? = null

        @SerializedName("sort")
         val sort: Int? = null

        @SerializedName("advTitle")
         val advTitle: String? = null

        @SerializedName("advImg")
         val advImg: String? = null

        @SerializedName("servModule")
         val servModule: String? = null

        @SerializedName("targetId")
         val targetId: Int? = null

        @SerializedName("type")
         val type: String? = null

        class ParamsDTO
    }
}