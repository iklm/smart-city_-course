package com.example.smartcity.ui.notifications.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.smartcity.databinding.RePersonSmbsOrderlistBinding
import com.example.smartcity.logic.base.BaseRecyclerAdapter
import com.example.smartcity.ui.notifications.model.SmartBusOrderListModel

/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/10/6 13:44
 *  created: Ikun
 *  Email: 2506789532@qq.com
 *
 *  智慧巴士 - 订单列表适配器
 **/
class SmartBusOrderListAdapter(
    // 数据源
    val list: MutableList<SmartBusOrderListModel>,
    // 布局ID
    val layoutID: Int
) : BaseRecyclerAdapter<SmartBusOrderListModel, RePersonSmbsOrderlistBinding>(list, layoutID){
    override fun setData(
        mBinding: RePersonSmbsOrderlistBinding,
        data: SmartBusOrderListModel,
        position: Int,
        holder: ViewHolder
    ) {
        // 获取数据填充视图
        list[position].apply {
            // 订单号
            mBinding.tvOrderNum.text = this.smsOrderNum
            // 订单状态
            if (this.smsStatus) {
                // 隐藏按钮
                mBinding.btnStatus.visibility = View.GONE
                mBinding.tvStatus.text = "已支付"
            } else {
                // 隐藏支付时间
                mBinding.tvPayTime.visibility = View.GONE
                // 更改按钮文本
                mBinding.btnStatus.text = "支付"
                mBinding.tvStatus.text = "未支付"
            }
            // 路线名称
            mBinding.tvPath.text = "路线: ${this.smsLineName}"
            // 起点到终点
            mBinding.tvStartAndEnd.text = "起点/终点: ${this.smsStartAndEnd}"
            // 价格
            mBinding.tvPrice.text = "价格: ${this.smsPrice.toString()}"
            // 用户姓名
            mBinding.tvUserName.text = "乘客姓名: ${this.smsUserName}"
            // 用户联系方式
            mBinding.tvUserTel.text = "联系方式: ${this.smsUserTel}"
            // 支付时间
            mBinding.tvPayTime.text = "支付时间: ${this.smsPayTime}"
        }
    }

    override fun getInflate(
        layoutInflater: LayoutInflater,
        parent: ViewGroup
    ): RePersonSmbsOrderlistBinding {
        return RePersonSmbsOrderlistBinding.inflate(layoutInflater, parent, false)
    }

}