package com.example.smartcity.logic.model

import com.google.gson.annotations.SerializedName

/**
 * 查询用户基本信息实体类
 */
class UserContentModel {
    /**
     * msg : 操作成功
     * code : 200
     * user : {"userId":2,"userName":"test01","nickName":"测试用户 01","email":"ljxl@qq.com","phonenumber":"13800000000","sex":"0","avatar":"","idCard":"210211199909090014","balance":9800,"score":10000}
     */
    @SerializedName("msg")
    var msg: String? = null

    @SerializedName("code")
    var code: Int? = null

    @SerializedName("user")
    var user: UserDTO? = null

    class UserDTO {
        /**
         * userId : 2
         * userName : test01
         * nickName : 测试用户 01
         * email : ljxl@qq.com
         * phonenumber : 13800000000
         * sex : 0
         * avatar :
         * idCard : 210211199909090014
         * balance : 9800
         * score : 10000
         */
        @SerializedName("userId")
        var userId: Int? = null

        @SerializedName("userName")
        var userName: String? = null

        @SerializedName("nickName")
        var nickName: String? = null

        @SerializedName("email")
        var email: String? = null

        @SerializedName("phonenumber")
        var phonenumber: String? = null

        @SerializedName("sex")
        var sex: String? = null

        @SerializedName("avatar")
        var avatar: String? = null

        @SerializedName("idCard")
        var idCard: String? = null

        @SerializedName("balance")
        var balance: Int? = null

        @SerializedName("score")
        var score: Int? = null
    }
}