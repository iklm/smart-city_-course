package com.example.smartcity.logic.network

import com.example.smartcity.logic.model.*
import okhttp3.RequestBody
import retrofit2.http.*

/**
 *
 *  ANDROID STUDIO version 2022.1
 *  codeAuthor: ikun
 *  email/qq: 2506789532@qq.com
 *  createTime: 2023/5/6 22:36
 *  ------------------------------
 *
 **/

/**
 * 这个接口定义了一些用于从服务器获取数据的网络请求方法。
 */
interface SmartApi {


    // 获取轮播图
    @GET("/prod-api/api/rotation/list")
    suspend fun getBanner(@QueryMap map: Map<String, Int>) : BannerModel



    // 获取全部服务
    @GET("/prod-api/api/service/list")
    suspend fun getService() : ServiceModel



    // 获取新闻列表 - 热门新闻主题
    // 使用 @GET 注解指定接口的请求方式为 GET 请求，并指定 url 路径为 /prod-api/press/press/list
    // 使用 @Query 注解指定请求参数 hot，并传入参数值 hot，最终生成的 url 会是 /prod-api/press/press/list?hot=xxx
    // 使用 suspend 关键字标记该方法为挂起函数，以便在协程中调用它
    // 函数返回类型为 HotNewsModel 类型，表示该请求返回的数据类型是 HotNewsModel 类型
    // 传入’Y‘获取热门主题，’N‘获取全部新闻列表
    @GET("/prod-api/press/press/list")
    suspend fun getHotNews(@Query("hot") hot: String) : HotNewsModel


    // 获取新闻列表
    // type传入参数值获取新闻分类列表
    @GET("/prod-api/press/press/list")
    suspend fun getNewsAll(@Query("type") id: Int) : HotNewsModel


    // 搜索新闻列表标题
    @GET("/prod-api/press/press/list")
    suspend fun getSeek(@Query("title") title: String) : HotNewsModel


    // 获取新闻详细内容
    // 根据id来获取新闻内容详情
    @GET(" /prod-api/press/press/{id}")
    suspend fun getNewsInfo(@Path("id") id: Int) : NewsInfoModel

    // 登录
    @POST("/prod-api/api/login")
    suspend fun getLogin(@Body body: RequestBody) : LoginModel

    // 用户个人基本信息
    @GET("/prod-api/api/common/user/getInfo")
    suspend fun getUserInfo(@Header("Authorization") token: String) : UserInfoALlModel

    // 修改个人信息
    @PUT("/prod-api/api/common/user")
    suspend fun getPUTUserInfo(@Header("Authorization") token: String,
                               @Body body: RequestBody) : LoginModel

    // 修改用户密码
    @PUT("/prod-api/api/common/user/resetPwd")
    suspend fun getPUTUserPasswd(@Header("Authorization") token: String,
                                 @Body body: RequestBody) : LoginModel


    // 新增意见反馈
    @POST("/prod-api/api/common/feedback")
    suspend fun getPUTFeedback(@Header("Authorization") token: String,
                                @Body body: RequestBody) : LoginModel
}