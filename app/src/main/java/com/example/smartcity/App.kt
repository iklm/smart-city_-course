package com.example.smartcity

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context

/**
 *
 *  ANDROID STUDIO version 2022.1
 *  codeAuthor: ikun
 *  email/qq: 2506789532@qq.com
 *  createTime: 2023/5/6 23:20
 *  ------------------------------
 *
 **/

/**
 * companion object定义了一个具有静态作用域的对象，因此可以通过类名来访问其成员。
 * 在此例中，声明了一个静态的context变量，用于保存应用程序上下文。
 * 同时，还声明了一个静态的getToken方法，该方法获取保存在SharedPreferences中的用户Token，
 * 并返回它（如果非空），否则返回“请先登录！”。
 */
class App : Application() {

    /**
     * @SuppressLint("StaticFieldLeak")表示解除对静态变量泄漏的警告，因为通常会建议将静态变量与ApplicationContext一起使用，
     * 以避免内存泄漏。但在这里，我们需要在静态变量中保存应用程序上下文，因此取消警告
     */
    companion object {

        // 定义一个url，方便我们以后调用服务器地址
        const val url = "http://124.93.196.45:10001/"

        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context

        // 该方法获取保存在SharedPreferences中的用户Token，
        // 并返回它（Token），否则返回“请先登录！”。
        val getToken: String
            get() {
                return context.getSharedPreferences("token", MODE_PRIVATE).getString("token", "")
                    .toString()
            }

    }

    /**
     * onCreate方法是在应用程序被创建时调用的，用于初始化应用程序的上下文。在此例中，将应用程序的上下文赋值给静态变量context，以便在其他地方使用。
     */
    override fun onCreate() {
        super.onCreate()
        context = applicationContext

    }
}