package com.example.smartcity.ui.home.model

/**
 *
 *  ANDROID STUDIO version 2022.1
 *  codeAuthor: ikun
 *  email/qq: 2506789532@qq.com
 *  createTime: 2023/5/7 16:57
 *  ------------------------------
 *  数据填充对象
 **/
// HomeService 是一个数据填充对象，用于保存服务数据
class HomeService(
    // 定义三个属性：是否显示全部服务，image 代表服务的图片 URL，title 代表服务的名称
    val isOK: Boolean,
    val image: String,
    val title: String
)