package com.example.smartcity.ui.SmartCity.model

/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/9/15 23:09
 *  created: Ikun
 *  Email: 2506789532@qq.com
 *
 *  中国智造-厂商招聘列表实体类
 *
 *  @param tvTitle 招聘标题
 *  @param tvMoney 招聘薪资
 *  @param tvContent 招聘内容详情
 **/
data class mOneMoodel (
    val tvTitle: String,
    val tvMoney: String,
    val tvContent: String
)