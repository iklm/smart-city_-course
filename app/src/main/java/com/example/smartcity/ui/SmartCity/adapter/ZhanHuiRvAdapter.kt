package com.example.smartcity.ui.SmartCity.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.smartcity.databinding.ReZhanhuiItemBinding
import com.example.smartcity.logic.base.BaseRecyclerAdapter
import com.example.smartcity.ui.SmartCity.model.ZhanHuiRvModel

/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/9/14 23:09
 *  created: Ikun
 *  Email: 2506789532@qq.com
 *
 *  中国智造主页-展会活动-列表适配器
 **/
class ZhanHuiRvAdapter(
    // 数据源
    private val list: MutableList<ZhanHuiRvModel>,
    // 布局id
    private val layoutId: Int
) : BaseRecyclerAdapter<ZhanHuiRvModel, ReZhanhuiItemBinding>(list, layoutId){
    override fun setData(
        mBinding: ReZhanhuiItemBinding,
        data: ZhanHuiRvModel,
        position: Int,
        holder: ViewHolder
    ) {
        // 获取数据
        val d = list[position]

        // 填充数据
        mBinding.zImg.setImageResource(d.zImg) // 图片
        mBinding.zTitle.text = d.zTitle // 标题/内容
    }


    // 绑定视图
    override fun getInflate(
        layoutInflater: LayoutInflater,
        parent: ViewGroup
    ): ReZhanhuiItemBinding {
        return ReZhanhuiItemBinding.inflate(layoutInflater, parent, false)
    }

}