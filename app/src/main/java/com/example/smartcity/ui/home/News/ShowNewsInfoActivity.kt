package com.example.smartcity.ui.home.News

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.view.MenuItem
import android.view.View
import com.example.smartcity.App.Companion.url
import com.example.smartcity.databinding.ActivityShowNewsInfoBinding
import com.example.smartcity.logic.Repository.api
import com.example.smartcity.logic.Repository.coroutine
import com.example.smartcity.logic.util.Util.glide
import com.example.smartcity.logic.util.Util.show

/**
 * 新闻详情类
 */
// 指定该 Activity 继承自 AppCompatActivity 类
class ShowNewsInfoActivity : AppCompatActivity() {

    // 定义视图绑定对象
    lateinit var mBinding: ActivityShowNewsInfoBinding

    // 在 onCreate 方法中进行 Activity 的初始化操作
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // 显示返回按钮
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        // 使用视图绑定库来绑定布局文件，并设置 Activity 的布局
        mBinding = ActivityShowNewsInfoBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        // 设置 Activity 的标题
        title = "新闻详情"



        // 通过 intent 获取传递过来的新闻 id
        val id = intent.getStringExtra("id")

        // 当新闻 id 不为空时，开启一个协程去获取该新闻的详细信息
        id?.let {
            coroutine {
                getNewsInfo(id.toInt())
            }
        }

    }

    /**
     * 协程函数，用于从 API 中获取指定 ID 的新闻的详细信息
     */
    private suspend fun getNewsInfo(id: Int) {
        // 调用 API 接口获取新闻详细信息
        val result = api.getNewsInfo(id)
        if (result.code == 200){
            runOnUiThread {
                // 若 API 调用成功，则使用获取到的数据填充 UI 控件
                result.data?.let {
                    mBinding.showNewsInfoTitle.text = it.title
                    mBinding.showNewsInfoDate.text = "发布时间: ${it.publishDate}"
                    mBinding.showNewsInfoUserSize.text = "${it.likeNum}评论"
                    glide(url + it.cover, mBinding.showNewsInfoImage)
                    mBinding.showNewsInfoContent.text = Html.fromHtml(it.content)
                }
                // 加载成功后显示布局
                mBinding.showNewsInfoLayout.visibility = View.VISIBLE
            }
        }else {
            // 若 API 调用失败，则在 UI 线程中展示错误提示信息
            runOnUiThread{
                "加载内容失败！".show()
            }
        }
    }

    /**
     * 当用户点击返回按钮时，销毁该页面
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}