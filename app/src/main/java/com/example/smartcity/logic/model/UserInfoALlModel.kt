package com.example.smartcity.logic.model

import com.google.gson.annotations.SerializedName

/**
 * 个人信息实体类
 */
class UserInfoALlModel {
    /**
     * msg : 操作成功
     * code : 200
     * user : {"userId":1113283,"userName":"ikun","nickName":"ikun","email":"2506789532@qq.com","phonenumber":"12345678***","sex":"0","avatar":"/profile/upload/2023/03/11/1bf328e1-ed60-4fff-a172-03c992dae3f2.jpg","idCard":"999666555222214","balance":3.993135592E7,"score":250}
     */
    @SerializedName("msg")
    var msg: String? = null

    @SerializedName("code")
    var code: Int? = null

    @SerializedName("user")
    var user: UserDTO? = null

    class UserDTO {
        /**
         * userId : 1113283
         * userName : ikun
         * nickName : ikun
         * email : 2506789532@qq.com
         * phonenumber : 12345678***
         * sex : 0
         * avatar : /profile/upload/2023/03/11/1bf328e1-ed60-4fff-a172-03c992dae3f2.jpg
         * idCard : 999666555222214
         * balance : 3.993135592E7
         * score : 250
         */
        @SerializedName("userId")
        var userId: Int? = null

        @SerializedName("userName")
        var userName: String? = null

        @SerializedName("nickName")
        var nickName: String? = null

        @SerializedName("email")
        var email: String? = null

        @SerializedName("phonenumber")
        var phonenumber: String? = null

        @SerializedName("sex")
        var sex: String? = null

        @SerializedName("avatar")
        var avatar: String? = null

        @SerializedName("idCard")
        var idCard: String? = null

        @SerializedName("balance")
        var balance: Double? = null

        @SerializedName("score")
        var score: Int? = null
    }
}