package com.example.smartcity.logic.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 *
 *  ANDROID STUDIO version 2022.1
 *  codeAuthor: ikun
 *  email/qq: 2506789532@qq.com
 *  createTime: 2023/5/6 22:32
 *  ------------------------------
 *
 **/
/**
 * 这个对象用于创建Retrofit实例，其中包含一个Http请求的基本URL和Gson转换器。
 */
object NetService {

    // Http请求的基本URL
    private const val BASE_URL = "http://124.93.196.45:10001/"

    // 创建Retrofit实例
    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    /**
     * 用于创建给定服务类的实例。
     *
     * @param serviceClass 要创建的服务类。
     * @return 给定服务类的实例（使用Retrofit创建）。
     */
    fun <E> create(serviceClass: Class<E>) : E = retrofit.create(serviceClass)

    /**
     * 在不指定服务类的情况下，使用内联函数来创建服务类的实例。
     *
     * @param R 要创建实例的服务类。
     * @return 指定服务类的实例（使用Retrofit创建）。
     */
    inline fun <reified R> create() : R = create(R::class.java)
}