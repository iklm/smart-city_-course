package com.example.smartcity.logic.model.SmartBusModel

import com.google.gson.annotations.SerializedName




/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/9/28 23:49
 *  created: Ikun
 *  Email: 2506789532@qq.com
 *
 *  智慧巴士 - 查询路线详情实体类
 **/
class SmartBusLineMsgModel {
    @SerializedName("msg")
    var msg: String? = null

    @SerializedName("code")
    var code: Int? = null

    @SerializedName("data")
    var data: DataDTO? = null

    class DataDTO {
        @SerializedName("searchValue")
        var searchValue: Any? = null

        @SerializedName("createBy")
        var createBy: Any? = null

        @SerializedName("createTime")
        var createTime: String? = null

        @SerializedName("updateBy")
        var updateBy: Any? = null

        @SerializedName("updateTime")
        var updateTime: String? = null

        @SerializedName("remark")
        var remark: Any? = null

        @SerializedName("params")
        var params: ParamsDTO? = null

        @SerializedName("id")
        var id: Int? = null

        @SerializedName("name")
        var name: String? = null

        @SerializedName("first")
        var first: String? = null

        @SerializedName("end")
        var end: String? = null

        @SerializedName("startTime")
        var startTime: String? = null

        @SerializedName("endTime")
        var endTime: String? = null

        @SerializedName("price")
        var price: Int? = null

        @SerializedName("mileage")
        var mileage: String? = null

        class ParamsDTO {}
    }
}