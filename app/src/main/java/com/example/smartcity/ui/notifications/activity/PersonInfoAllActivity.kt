package com.example.smartcity.ui.notifications.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.example.smartcity.App.Companion.getToken
import com.example.smartcity.R
import com.example.smartcity.databinding.ActivityPersonInfoAllBinding
import com.example.smartcity.logic.Repository.api
import com.example.smartcity.logic.Repository.coroutine
import com.example.smartcity.logic.util.Util.glide
import com.example.smartcity.logic.util.Util.show
import kotlinx.coroutines.Dispatchers
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject

/**
 * 个人信息展示
 */
class PersonInfoAllActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityPersonInfoAllBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityPersonInfoAllBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        // 设置标题
        title = "个人信息"
        // 显示回退按钮
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        // 显示个人信息内容
        showPersonContent()

        // 点击提交修改信息代码
        mBinding.personAllPUT.setOnClickListener {
            putUserInfo()
        }
    }

    /**
     * 显示个人信息内容
     */
    private fun showPersonContent() = coroutine(Dispatchers.Main) {

        // 先判断输入框是否为空 （比赛可以忽略， 裁判可能没时间测试， 一般只修改一个昵称和性别就打分了）
        // 想写的话可以自己写if判断


        // 获取用户基本信息， 需要我们传入token
        api.getUserInfo(getToken).apply {
            if (this.code == 200) {
                // 将数据填充到控件上
                runOnUiThread {
                    this.user.apply {
                        // 显示用户头像
                        glide("http://124.93.196.45:10002/" + this?.avatar,
                            mBinding.personAllImage)
                        // 显示用户昵称
                        mBinding.personAllName.setText(this?.nickName)
                        // 显示用户性别
                        if (this?.sex == "0") mBinding.personAllSex.setText("男")
                        else mBinding.personAllSex.setText("女")
                        // 显示用户联系电话
                        mBinding.personAllPhone.setText(this?.phonenumber)
                    }
                }
            } else runOnUiThread {
                this.msg?.show()
            }
        }
    }


    /**
     * 提交个人信息
     */
    private fun putUserInfo() = coroutine(Dispatchers.Main) {
        // 提交请求体
        val result = api.getPUTUserInfo(getToken, JSONObject().apply {
            put("avatar", "可以自己下去写写上去文件（这个是上去头像）")
            put("nickName", mBinding.personAllName.text)
            put("phonenumber", mBinding.personAllPhone.text)
            if (mBinding.personAllSex.text.toString() == "男") put("sex", 0)
            else put("sex", 1)
        }.toString().toRequestBody("application/json".toMediaTypeOrNull()))

        // 判断是否提交成功
        if (result.code == 200) {
            // 如果提交成功侧重新启动删除该页面再启动
            finish()
            startActivity(Intent(this@PersonInfoAllActivity,
                PersonInfoAllActivity::class.java))
        }else {
            // 如果不成功侧提示消息
            runOnUiThread {
                result.msg?.show()
            }
        }
    }


    // 点击回退按钮销毁该页面
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}