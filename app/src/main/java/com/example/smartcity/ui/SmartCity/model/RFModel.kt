package com.example.smartcity.ui.SmartCity.model

/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/9/6 20:20
 *  created: Ikun
 *  Email: 2506789532@qq.com
 *  中国智造-推荐厂商列表数据填充实体类
 **/
data class RFModel(
    val rfImage: Int,
    val rfTitle: String,
    val rfContent: String
)
