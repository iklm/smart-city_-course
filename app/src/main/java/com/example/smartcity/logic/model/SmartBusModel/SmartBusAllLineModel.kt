package com.example.smartcity.logic.model.SmartBusModel

import com.google.gson.annotations.SerializedName

/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/9/18 23:39
 *  created: Ikun
 *  Email: 2506789532@qq.com
 *
 *  智慧巴士 - 线路列表实体类
 **/

class SmartBusAllLineModel {
    @SerializedName("total")
    var total: Int? = null

    @SerializedName("rows")
    var rows: List<RowsDTO>? = null

    @SerializedName("code")
    var code: Int? = null

    @SerializedName("msg")
    var msg: String? = null

    class RowsDTO {
        @SerializedName("searchValue")
        var searchValue: Any? = null

        @SerializedName("createBy")
        var createBy: Any? = null

        @SerializedName("createTime")
        var createTime: String? = null

        @SerializedName("updateBy")
        var updateBy: Any? = null

        @SerializedName("updateTime")
        var updateTime: String? = null

        @SerializedName("remark")
        var remark: Any? = null

        @SerializedName("params")
        var params: ParamsDTO? = null

        @SerializedName("id")
        var id: Int? = null

        @SerializedName("name")
        var name: String? = null

        @SerializedName("first")
        var first: String? = null

        @SerializedName("end")
        var end: String? = null

        @SerializedName("startTime")
        var startTime: String? = null

        @SerializedName("endTime")
        var endTime: String? = null

        @SerializedName("price")
        var price: Int? = null

        @SerializedName("mileage")
        var mileage: String? = null

        class ParamsDTO {

        }
    }
}