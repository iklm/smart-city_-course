package com.example.smartcity.logic.model.SmartBusModel

import com.google.gson.annotations.SerializedName

/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/10/4 22:45
 *  created: Ikun
 *  Email: 2506789532@qq.com
 *
 *  智慧巴士 订单列表实体类
 **/
class SmartBusOrderListModel {
    @SerializedName("total")
    var total: Int? = null

    @SerializedName("rows")
    var rows: List<RowsDTO>? = null

    @SerializedName("code")
    var code: Int? = null

    @SerializedName("msg")
    var msg: String? = null

    class RowsDTO {
        @SerializedName("searchValue")
        var searchValue: Any? = null

        @SerializedName("createBy")
        var createBy: Any? = null

        @SerializedName("createTime")
        var createTime: String? = null

        @SerializedName("updateBy")
        var updateBy: Any? = null

        @SerializedName("updateTime")
        var updateTime: String? = null

        @SerializedName("remark")
        var remark: Any? = null

        @SerializedName("params")
        var params: ParamsDTO? = null

        @SerializedName("id")
        var id: Int? = null

        @SerializedName("orderNum")
        var orderNum: String? = null

        @SerializedName("path")
        var path: String? = null

        @SerializedName("start")
        var start: String? = null

        @SerializedName("end")
        var end: String? = null

        @SerializedName("price")
        var price: Int? = null

        @SerializedName("userName")
        var userName: String? = null

        @SerializedName("userTel")
        var userTel: String? = null

        @SerializedName("userId")
        var userId: Int? = null

        @SerializedName("status")
        var status: Int? = null

        @SerializedName("paymentType")
        var paymentType: String? = null

        @SerializedName("payTime")
        var payTime: String? = null

        class ParamsDTO {}
    }
}