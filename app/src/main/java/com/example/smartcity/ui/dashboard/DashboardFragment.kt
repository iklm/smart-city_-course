package com.example.smartcity.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.example.smartcity.R
import com.example.smartcity.databinding.FragmentDashboardBinding
import com.example.smartcity.logic.Repository
import com.example.smartcity.logic.Repository.coroutine
import com.example.smartcity.logic.util.Util.show
import com.example.smartcity.ui.home.adapter.HomeMoreRecyclerAdapter
import com.example.smartcity.ui.home.model.HomeService

/**
 * 底部导航栏 - 全部服务
 * TODO 显示智慧城市所有服务模块
 */
class DashboardFragment : Fragment() {

    private var _binding: FragmentDashboardBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        return binding.root
    }


    /**
     * 在这里编写代码
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        coroutine {
            getService()
        }
    }

    // 定义一个 ArrayList 变量 arrayService 用于保存获取到的数据
    private var arrayService = ArrayList<HomeService>()

    // 定义一个挂起函数 getService，用于调用接口获取数据并更新 RecyclerView 的适配器
    private suspend fun getService() {
        // 调用 api.getService() 方法获取服务数据
        val result = Repository.api.getService()
        // 清空之前的数据
        arrayService.clear()
        // 如果状态码为 200，说明获取成功，将数据添加到 arrayService 列表中，并更新 RecyclerView 的适配器
        if (result.code == 200) {
            // 将获取到的数据添加到 arrayService 中
            for (i in result.rows!!) {
                arrayService.add(HomeService(true, i.imgUrl!!, i.serviceName!!))
            }

            // 在 UI 线程中设置 RecyclerView 的属性和适配器
            activity?.runOnUiThread {
                // 设置 isNestedScrollingEnabled 属性为 false，确保滑动性能良好
                binding.AllServiceRecyclerView.isNestedScrollingEnabled = false
                // 设置 layoutManager 为 GridLayoutManager，以网格布局方式显示子项视图
                binding.AllServiceRecyclerView.layoutManager = GridLayoutManager(context, 3)
                // 设置 adapter 为 HomeMoreRecyclerAdapter，并传入数据列表 arrayService 和子项布局文件 re_home_item
                binding.AllServiceRecyclerView.adapter =
                    HomeMoreRecyclerAdapter(this, arrayService, R.layout.re_home_item, arrayService.size)
                // 更新 RecyclerView 的适配器
                binding.AllServiceRecyclerView.adapter?.notifyItemChanged(arrayService.size)
            }
        } else {
            // 否则提示加载服务失败
            // 在 UI 线程中弹出提示框
            activity?.runOnUiThread {
                "加载服务失败！".show()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}