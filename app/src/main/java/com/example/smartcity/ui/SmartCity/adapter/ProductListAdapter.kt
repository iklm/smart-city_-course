package com.example.smartcity.ui.SmartCity.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.smartcity.databinding.ReChinabuildProductlistItemBinding
import com.example.smartcity.logic.base.BaseRecyclerAdapter
import com.example.smartcity.ui.SmartCity.Activity.ProductListShowActivity
import com.example.smartcity.ui.SmartCity.model.ProductListModel

/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/9/13 18:17
 *  created: Ikun
 *  Email: 2506789532@qq.com
 *
 *  中国智造主页-产品列表模块适配器
 **/
class ProductListAdapter(
    // 数据源
    val list: MutableList<ProductListModel>,
    // 布局ID
    val layoutID: Int
) : BaseRecyclerAdapter<ProductListModel, ReChinabuildProductlistItemBinding>(list, layoutID){
    override fun setData(
        mBinding: ReChinabuildProductlistItemBinding,
        data: ProductListModel,
        position: Int,
        holder: ViewHolder
    ) {
        val d = list[position]

        // 填充数据
        mBinding.pTitle.text = d.pTitle
        mBinding.pImg.setImageResource(d.pImg)

        // 点击跳转到产品列表
        holder.itemView.apply {
            val context = this.context
            this.setOnClickListener {
                context.startActivity(Intent(context, ProductListShowActivity::class.java))
            }
        }
    }

    override fun getInflate(
        layoutInflater: LayoutInflater,
        parent: ViewGroup
    ): ReChinabuildProductlistItemBinding {
        return ReChinabuildProductlistItemBinding.inflate(layoutInflater, parent, false)
    }

}