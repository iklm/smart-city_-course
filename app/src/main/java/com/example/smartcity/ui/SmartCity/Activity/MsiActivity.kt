package com.example.smartcity.ui.SmartCity.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.example.smartcity.R
import com.example.smartcity.databinding.ActivityMsiBinding
import com.example.smartcity.logic.util.Util.show

/**
 * 中国智造主页-厂商入驻
 */
class MsiActivity : AppCompatActivity() {

    lateinit var mBinding: ActivityMsiBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityMsiBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        // 显示回退按钮
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        // 标题
        title = "厂商入驻"

        // 当点击提交时清空EditText内容
        mBinding.b1.setOnClickListener {
            mBinding.e1.setText("")
            mBinding.e2.setText("")
            mBinding.e3.setText("")
            mBinding.e4.setText("")

            // 来一个提示提交成功
            "提交成功".show()
        }

    }

    /**
     * 点击回退按钮时销毁该界面
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}