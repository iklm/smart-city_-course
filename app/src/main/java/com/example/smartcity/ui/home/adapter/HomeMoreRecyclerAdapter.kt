package com.example.smartcity.ui.home.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.smartcity.App.Companion.context
import com.example.smartcity.App.Companion.url
import com.example.smartcity.R
import com.example.smartcity.databinding.ReHomeItemBinding
import com.example.smartcity.logic.base.BaseRecyclerAdapter
import com.example.smartcity.logic.model.ServiceModel
import com.example.smartcity.logic.util.Util.glide
import com.example.smartcity.logic.util.Util.show
import com.example.smartcity.ui.AllService.SmartBus.SmartBusActivity
import com.example.smartcity.ui.home.HomeFragment
import com.example.smartcity.ui.home.model.HomeService
import com.google.android.material.bottomnavigation.BottomNavigationView

/**
 *
 *  ANDROID STUDIO version 2022.1
 *  codeAuthor: ikun
 *  email/qq: 2506789532@qq.com
 *  createTime: 2023/5/7 12:34
 *  ------------------------------
 *  主页推荐服务适配器
 **/

// HomeMoreRecyclerAdapter 是一个 RecyclerView 的适配器类
class HomeMoreRecyclerAdapter(
    private val fr: Fragment,
    // 适配器的构造函数需要传入一个包含数据的列表、布局文件的 ID 和显示数量
    private val list: MutableList<HomeService>,
    layoutId: Int,
    private val size: Int
) : BaseRecyclerAdapter<HomeService, ReHomeItemBinding>(list, layoutId) {

    // setData 方法用于设置每个子项视图中 UI 控件的数据
    override fun setData(mBinding: ReHomeItemBinding, data: HomeService, position: Int, holder: ViewHolder) {
        // 获取当前数据项并进行判断
        val d = list[position]
        if (position == 9) {
            if (d.isOK) {
                // 如果是全部服务子项，则填充图片和文本数据
                glide(url + d.image, mBinding.homeMoreImage)
                mBinding.homeMoreTitle.text = "全部服务"
            }
        } else {
            // 否则填充图片和文本数据
            glide(url + d.image, mBinding.homeMoreImage)
            mBinding.homeMoreTitle.text = d.title
        }

        // 设置子项视图的点击事件
        holder.itemView.setOnClickListener {


            // 判断 homeMoreTitle 的内容是否为 "全部服务"
            if (mBinding.homeMoreTitle.text.toString() == "全部服务") {
                // 获取父 Fragment 的 Activity 中的 BottomNavigationView 对象
                val nav = fr.requireActivity().findViewById<BottomNavigationView>(R.id.nav_view)
                // 将 BottomNavigationView 中“仪表盘”对应的 MenuItem 设为选中状态
                nav.selectedItemId = R.id.navigation_dashboard
            }


            // 判断 homeMoreTitle 的内容是否为 "智慧巴士"
            if (mBinding.homeMoreTitle.text.toString() == "智慧巴士") {
                // 跳转到智慧巴士Activity
                val con = holder.itemView.context
                con.startActivity(Intent(con, SmartBusActivity::class.java))
            }



        }

    }

    // getInflate 方法负责将布局文件转换为视图绑定对象
    override fun getInflate(layoutInflater: LayoutInflater, parent: ViewGroup): ReHomeItemBinding {
        // 使用 inflate 方法将布局文件转换为视图绑定对象
        return ReHomeItemBinding.inflate(layoutInflater, parent, false)
    }

    // getItemCount 方法返回显示数量
    override fun getItemCount(): Int = size
}