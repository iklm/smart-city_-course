package com.example.smartcity.logic

import android.app.Activity
import com.example.smartcity.logic.network.NetService.create
import com.example.smartcity.logic.network.SmartApi
import com.example.smartcity.logic.network.SmartBusApi
import kotlinx.coroutines.*
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

/**
 *
 *  ANDROID STUDIO version 2022.1
 *  codeAuthor: ikun
 *  email/qq: 2506789532@qq.com
 *  createTime: 2023/5/6 22:12
 **/
/**
 * 这个对象表示应用程序的仓库（repository），它提供了一个单例SmartApi接口实例以及一些协程方法。
 */
object Repository {

    // 获取SmartApi接口的单例
    val api = create<SmartApi>()

    // 获取智慧巴士接口
    val sApi = create<SmartBusApi>() //  创建用于进行智慧巴士相关网络请求的SmartBusApi实例

    /**
     * 创建一个新的协程，并将其运行在指定的上下文中。默认情况下，该函数在IO调度器上启动一个新的协程。
     *
     * @param context 协程应运行的上下文，可以通过Dispatchers类中的静态字段来指定要使用的调度器。默认为IO调度器。
     * @param block 要在协程中执行的代码块，该代码块必须定义为suspend函数类型。
     */
    fun coroutine(
        context: CoroutineContext = Dispatchers.IO,
        block: suspend CoroutineScope.() -> Unit
    ) : Job {
        return CoroutineScope(context).launch {
            try {
                coroutineScope {
                    block()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }



    // 智慧巴士存储Activity
    // 这段代码声明了一个ArrayList变量smartArray，它用于存储Activity对象。
    val smartArray = ArrayList<Activity>()

    // 添加活动
    // 这是一个用于将Activity添加到smartArray中的方法。
    // 它接受一个Activity对象作为参数，并将其添加到smartArray中。
    fun smartAddActivity(activity: Activity) {
        smartArray.add(activity)
    }

    // 移除所有活动
    /*
    这个方法用于移除所有已经完成（即将被销毁）的Activity。
    它遍历smartArray中的所有Activity对象，检查它们是否已经被标记为“正在完成”（isFinishing为true），
    如果是的话，就调用finish()方法来销毁该Activity。
     */
    fun smartMoveAllActivity() {
        for (i in smartArray) {
            if (!i.isFinishing) {
                i.finish()
            }
        }
    }


}