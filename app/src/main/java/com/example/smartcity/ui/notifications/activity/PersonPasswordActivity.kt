package com.example.smartcity.ui.notifications.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.example.smartcity.R
import com.example.smartcity.databinding.ActivityPersonPasswordBinding
import com.example.smartcity.logic.Repository.api
import com.example.smartcity.logic.Repository.coroutine
import kotlinx.coroutines.Dispatchers
import com.example.smartcity.App
import com.example.smartcity.App.Companion.getToken
import com.example.smartcity.logic.util.Util.show
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject

/**
 * 个人中心修改密码页面
 */
class PersonPasswordActivity : AppCompatActivity() {


    private lateinit var mBinding: ActivityPersonPasswordBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityPersonPasswordBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = "修改密码"


        // 点击修改密码
        mBinding.BtnPasswd.setOnClickListener {
            // 修改密码函数
            revampPasswd()
        }
    }


    /**
     * 修改密码
     */
    private fun revampPasswd() = coroutine {
        api.getPUTUserPasswd(getToken, JSONObject().apply {
            // 以键值对的方式进行封装请求体
            put("oldPassword", mBinding.Passwd.text) // 原密码
            put("newPassword", mBinding.NewPasswd.text) // 新密码
        }.toString().toRequestBody("application/json".toMediaType())).apply {
            // 判断修改状态
            if (this.code == 200) {
                // 返回200 表示成功
                // 成功我们就提示就可以了， 不用把本地用户消除， 然后重新登录
                runOnUiThread {
                    "修改成功!".show()
                    // 消除输入框
                    mBinding.Passwd.setText("")
                    mBinding.NewPasswd.setText("")
                }
            } else runOnUiThread {
                // 如果返回的是其他的我们就提示 msg就可以了
                this.msg?.show()
            }
        }
    }



    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}