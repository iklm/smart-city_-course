package com.example.smartcity.ui.SmartCity

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.smartcity.App
import com.example.smartcity.R
import com.example.smartcity.databinding.FragmentChinaBuildBinding
import com.example.smartcity.logic.Repository
import com.example.smartcity.logic.Repository.coroutine
import com.example.smartcity.logic.model.BannerModel
import com.example.smartcity.logic.util.Util
import com.example.smartcity.logic.util.Util.show
import com.example.smartcity.ui.SmartCity.adapter.ChinaBuildMoreAdapter
import com.example.smartcity.ui.SmartCity.fragment.productFragment
import com.example.smartcity.ui.SmartCity.fragment.recommendFirmFragment
import com.example.smartcity.ui.SmartCity.model.ChinaBuildModel
import com.example.smartcity.ui.home.News.ShowNewsInfoActivity
import com.example.smartcity.ui.home.adapter.HomeMoreRecyclerAdapter
import com.example.smartcity.ui.home.model.HomeService
import com.google.android.material.tabs.TabLayoutMediator
import com.youth.banner.adapter.BannerImageAdapter
import com.youth.banner.holder.BannerImageHolder
import com.youth.banner.indicator.CircleIndicator
import kotlin.coroutines.coroutineContext

/**
 * 中国智造
 */
class ChinaBuildFragment : Fragment() {

    private var _bind: FragmentChinaBuildBinding? = null
    private val binding: FragmentChinaBuildBinding get() = _bind!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _bind = FragmentChinaBuildBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // 调用函数
        chinaBuildBanner()
        chinaBuildModule()
        recommendFirm()
    }


    /**
     * 轮播图
     * 轮播图获取是要通过协程的哦
     */
    // 定义了一个存储轮播图数据的列表arrayBanner，类型为ArrayList<BannerModel.RowsDTO>。
    var arrayBanner = ArrayList<BannerModel.RowsDTO>()
    private fun chinaBuildBanner() = coroutine {
        // 使用Retrofit库生成的接口api来获取轮播图数据。调用了getBanner方法，并通过mapOf方法封装查询参数。
        val result = Repository.api.getBanner(mapOf("pageNum" to 1, "pageSize" to 8, "type" to 2))

        //判断服务器返回的数据是否正确。如果返回码为200，则将数据加入到arrayBanner中，并通过UI线程更新UI界面显示轮播图；否则，在UI线程中弹出提示信息。
        if (result.code == 200) {
            // 清空原先数据
            arrayBanner.clear()
            arrayBanner.addAll(result.rows!!)
            activity?.runOnUiThread {

                // Banner适配器
                binding.cBanner.apply { // 使用Kotlin中的扩展函数apply对binding.homeBanner进行设置。

                    //使用BannerImageAdapter适配器将轮播图数据加载到homeBanner中，并设置圆角、指示器等属性。
                    this.adapter = object : BannerImageAdapter<BannerModel.RowsDTO>(arrayBanner) {

                        //重写了BannerImageAdapter中的抽象方法，用于加载轮播图图片。在这里，使用Glide库加载图片，并根据服务器地址和图片名称拼接URL。
                        override fun onBindView(
                            p0: BannerImageHolder?,
                            p1: BannerModel.RowsDTO?,
                            p2: Int,
                            p3: Int
                        ) {
                            // 在app对象里面定义一个url，方便我们以后调用服务器地址
                            // const val url = "http://124.93.196.45:10001/"
                            Util.glide(App.url + p1?.advImg, p0?.imageView!!)
                        }
                    }
                    // 添加生命周期观察者，当页面销毁时自动停止轮播图，并设置指示器样式。
                    this.addBannerLifecycleObserver(this@ChinaBuildFragment).indicator =
                        CircleIndicator(context)
                    // 设置轮播图的圆角为25像素。
                    this.setBannerRound(25f)

                    this.setOnBannerListener { any, i ->
                        // 为 banner 设置点击监听器
                        // 根据索引获取我们点击的照片对应的新闻 id
                        val newsId = when (i) {
                            0 -> 25
                            1 -> 26
                            else -> 27
                        }

                        // 启动 ShowNewsInfoActivity 并将该新闻 id 以 "id" 为 key 进行传递
                        startActivity(
                            Intent(context, ShowNewsInfoActivity::class.java)
                                .putExtra("id", newsId.toString())
                        )
                    }
                }
            }
        } else {
            activity?.runOnUiThread {
                "加载轮播图出错！".show()
            }
        }
    }


    /**
     * 四个小模块
     */
    // 定义一个 ArrayList 变量 arrayService 用于保存获取到的数据
    private var arrayService = ArrayList<ChinaBuildModel>()
    private fun chinaBuildModule() {

        // 设置数据
        arrayService.add(ChinaBuildModel(R.mipmap.img1, "产品列表"))
        arrayService.add(ChinaBuildModel(R.mipmap.img2, "展会活动"))
        arrayService.add(ChinaBuildModel(R.mipmap.img3, "厂商招聘"))
        arrayService.add(ChinaBuildModel(R.mipmap.img4, "厂商入驻"))

        // 列表设配器
        binding.cChinaBuildMore.apply {

            // 设置 isNestedScrollingEnabled 属性为 false，确保滑动性能良好
            this.isNestedScrollingEnabled = false

            // 设置 layoutManager 为 GridLayoutManager，以网格布局方式显示子项视图
            this.layoutManager = GridLayoutManager(context, 4)

            // 设置 adapter 为 HomeMoreRecyclerAdapter，并传入数据列表 arrayService 和子项布局文件 re_home_item
            this.adapter = ChinaBuildMoreAdapter(arrayService, R.layout.re_home_item)

            // 更新 RecyclerView 的适配器
            this.adapter?.notifyItemChanged(arrayService.size)
        }

    }


    /**
     * 厂商列表
     */
    private fun recommendFirm() {
        // 配置TabLayout
        val tabTitle = listOf(
            "推荐厂商",
            "产品与解决方案"
        )

        // ViewPager2适配器
        binding.cViewPager2.adapter = object : FragmentStateAdapter(this) {

            // 有多少个Fragment
            override fun getItemCount(): Int = tabTitle.size

            // 根据位置显示Fragment
            override fun createFragment(position: Int): Fragment {
                return when (position) {
                    0 -> recommendFirmFragment()
                    1 -> productFragment()
                    else -> recommendFirmFragment()
                }
            }
        }

        TabLayoutMediator(binding.cTabLayout, binding.cViewPager2) { tab, position ->
            when(position) {
                0 -> tab.text = tabTitle[position]
                1 -> tab.text = tabTitle[position]
                else -> tab.text = "出错!"
            }
        }.attach()

    }


    override fun onDestroy() {
        super.onDestroy()
        _bind = null
    }
}