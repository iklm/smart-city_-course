package com.example.smartcity.logic.network

import com.example.smartcity.logic.model.SmartBusModel.SmartBusAllLineModel
import com.example.smartcity.logic.model.SmartBusModel.SmartBusLineMsgModel
import com.example.smartcity.logic.model.SmartBusModel.SmartBusOrderListModel
import com.example.smartcity.logic.model.SmartBusModel.SmartBusOrdersModel
import com.example.smartcity.logic.model.SmartBusModel.SmartBusSiteModel
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/9/18 23:30
 *  created: Ikun
 *  Email: 2506789532@qq.com
 *
 *  智慧巴士 - 接口Api
 **/
interface SmartBusApi {

    // 获取线路列表
    @GET("/prod-api/api/bus/line/list")
    suspend fun getSmartBusAllLIne() : SmartBusAllLineModel

    // 查询站点信息
    @GET("/prod-api/api/bus/stop/list")
    suspend fun getSite(@Query("linesId") linesID: Int) : SmartBusSiteModel

    // 查询路线详情
    @GET("/prod-api/api/bus/line/{id}")
    suspend fun getLineMsg(@Path("id") id: String) : SmartBusLineMsgModel

    // 新增巴士订单
    @POST("/prod-api/api/bus/order")
    suspend fun setBusOrders(@Header("Authorization") token: String,
                             @Body body: RequestBody) : SmartBusOrdersModel

    // 查询订单列表
    @GET("/prod-api/api/bus/order/list")
    suspend fun getBusOrderList(@Header("Authorization") token: String, @Query("status") statusNum: Int) : SmartBusOrderListModel

}