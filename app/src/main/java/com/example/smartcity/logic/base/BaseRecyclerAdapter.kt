package com.example.smartcity.logic.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding

/**
 *
 *  ANDROID STUDIO version 2022.1
 *  codeAuthor: ikun
 *  email/qq: 2506789532@qq.com
 *  createTime: 2023/5/6 22:16
 *  -------------------------------
 *
 **/


/**
 * 这是一个抽象类，用于创建RecyclerView.Adapter。泛型E表示列表中的数据类型，
 * 泛型VB表示使用ViewBinding生成的视图绑定类型。
 *
 * @param list 包含数据的可变列表。
 * @param layoutId 每个列表项的布局资源id。
 */
abstract class BaseRecyclerAdapter <E, VB: ViewBinding>(
    private val list: MutableList<E>,
    @LayoutRes
    private val layoutId: Int,
) : RecyclerView.Adapter<BaseRecyclerAdapter<E, VB>.ViewHolder>(){

    // 填充数据
    abstract fun setData(mBinding: VB, data: E, position: Int, holder: ViewHolder)

    // 获取布局
    abstract fun getInflate(layoutInflater: LayoutInflater, parent: ViewGroup) : VB


    override fun onBindViewHolder(holder: ViewHolder, position: Int)
            = holder.bindData(list[position], position)

    // 列表数量
    override fun getItemCount(): Int = list.size

    // 布局Id
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
            = ViewHolder(getInflate(LayoutInflater.from(parent.context), parent))

    inner class ViewHolder(private val viewBinding: VB) : RecyclerView.ViewHolder(viewBinding.root) {
        fun bindData(data: E, position: Int){
            setData(viewBinding, data, position, this)
        }
    }
}
