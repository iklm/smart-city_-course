package com.example.smartcity.ui.home.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.smartcity.App.Companion.url
import com.example.smartcity.databinding.ReHomeItemHotBinding
import com.example.smartcity.logic.base.BaseRecyclerAdapter
import com.example.smartcity.logic.model.HotNewsModel
import com.example.smartcity.logic.util.Util.glide
import com.example.smartcity.logic.util.Util.show
import com.example.smartcity.ui.home.News.ShowNewsInfoActivity

/**
 *
 *  ANDROID STUDIO version 2022.1
 *  codeAuthor: ikun
 *  email/qq: 2506789532@qq.com
 *  createTime: 2023/5/7 22:45
 *  ------------------------------
 *
 **/
// 定义一个名为 HomeHotRecyclerAdapter 的适配器类，继承自 BaseRecyclerAdapter 类
class HomeHotRecyclerAdapter(
    private val list: MutableList<HotNewsModel.RowsDTO>, // 数据列表
    private val layoutId: Int // 子项布局 id
) : BaseRecyclerAdapter<HotNewsModel.RowsDTO, ReHomeItemHotBinding>(list, layoutId){

    // 实现 setData 方法，在子项视图绑定数据
    override fun setData(
        mBinding: ReHomeItemHotBinding, // 视图绑定对象
        data: HotNewsModel.RowsDTO, // 当前子项对应的数据
        position: Int, // 当前子项在列表中的位置
        holder: ViewHolder // 父级视图持有者
    ) {
        val d = list[position] // 获取当前子项对应的数据

        // 填充数据
        glide(url + d.cover, mBinding.homeHotImageView) // 加载图片并设置到 ImageView 中
        mBinding.HomeHotTitle.text = d.title // 设置标题文本

        // 获取context
        val con = holder.itemView.context
        // 设置点击事件
        holder.itemView.setOnClickListener {
            // 跳转到下一个意图, 需要传入item子视图的id，这样下一个意图就可以根据id查找到相关的新闻详情了
            con.startActivity(Intent(con, ShowNewsInfoActivity::class.java)
                .putExtra("id",d.id.toString()))
        }
    }

    // 实现 getInflate 方法，在子项视图创建时返回视图绑定对象
    override fun getInflate(
        layoutInflater: LayoutInflater, // 视图加载器
        parent: ViewGroup // 父级 ViewGroup
    ): ReHomeItemHotBinding {
        // 返回通过 DataBinding 解析布局的视图绑定对象
        return ReHomeItemHotBinding.inflate(layoutInflater, parent, false)
    }
}