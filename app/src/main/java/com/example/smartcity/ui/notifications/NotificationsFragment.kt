package com.example.smartcity.ui.notifications

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.smartcity.App.Companion.getToken
import com.example.smartcity.databinding.FragmentNotificationsBinding
import com.example.smartcity.logic.Repository.api
import com.example.smartcity.logic.Repository.coroutine
import com.example.smartcity.logic.util.Util.glide
import com.example.smartcity.logic.util.Util.onClick
import com.example.smartcity.logic.util.Util.show
import com.example.smartcity.ui.notifications.activity.PersonFeedbackActivity
import com.example.smartcity.ui.notifications.activity.PersonInfoAllActivity
import com.example.smartcity.ui.notifications.activity.PersonLoginActivity
import com.example.smartcity.ui.notifications.activity.PersonPasswordActivity
import com.example.smartcity.ui.notifications.activity.SmartBusOrderListActivity
import kotlinx.coroutines.Dispatchers


/**
 * 底部导航栏 - 个人中心
 * TODO 显示用户信息 / 更改密码 / 意见反馈
 */
class NotificationsFragment : Fragment() {
    private val BTN_TEXT_1 = "登录"
    private val BTN_TEXT_2 = "退出"

    private var _binding: FragmentNotificationsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNotificationsBinding.inflate(inflater, container, false)
        return binding.root
    }

    /**
     * 在此处编写代码
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onRegister()
    }

    /**
     * 登录验证
     */
    @SuppressLint("UseRequireInsteadOfGet")
    private fun onRegister() {

        // 根据按钮的文本进入不同的界面
        binding.outInfoTv.setOnClickListener {
            // 登录
            // BTN_TEXT_1 登录
            // BTN_TEXT_2 退出
            if (binding.outInfoTv.text.toString() == BTN_TEXT_1) {
                startActivity(Intent(requireContext(), PersonLoginActivity::class.java))
            } else if (binding.outInfoTv.text.toString() == BTN_TEXT_2){
                removeUserInfo()
            }
        }

        /*
        // 点击进入个人信息页面
        binding.cardView4.setOnClickListener {
            // 判断用户是否已经登录
            if (getToken.isNotEmpty()) {
                // 不为空直接进入下一个页面展示用户信息
                startActivity(Intent(context, PersonInfoAllActivity::class.java))
            } else {
                "请先登录!".show()
            }
        }


        // 点击进入修改密码页面
        binding.cardView5.setOnClickListener {
            // 判断用户是否已经登录
            if (getToken.isNotEmpty()) {
                // 不为空直接进入下一个页面展示用户信息
                startActivity(Intent(context, PersonPasswordActivity::class.java))
            } else {
                "请先登录!".show()
            }
        }


        // 点击进入意见反馈
        binding.cardView7.setOnClickListener {
            // 判断用户是否已经登录
            if (getToken.isNotEmpty()) {
                // 不为空直接进入下一个页面展示用户信息
                startActivity(Intent(context, PersonFeedbackActivity::class.java))
            } else {
                "请先登录!".show()
            }
        }


        // 点击进入订单列表页面
        binding.cardView6.setOnClickListener {
            // 判断用户是否已经登录
            if (getToken.isNotEmpty()) {
                // 不为空直接进入下一个页面展示用户信息
                startActivity(Intent(context, PersonFeedbackActivity::class.java))
            } else {
                "请先登录!".show()
            }
        }
       */


        // 使用封装好的代码

        // 点击进入个人信息页面
        binding.cardView4.onClick(context!!, "请先登录！", PersonInfoAllActivity::class.java)

        // 点击进入修改密码页面
        binding.cardView5.onClick(context!!, "请先登录！", PersonPasswordActivity::class.java)

        // 点击进入意见反馈
        binding.cardView7.onClick(context!!, "请先登录！", PersonFeedbackActivity::class.java)

        // 点击进入订单列表页面
        binding.cardView6.onClick(context!!, "请先登录！", SmartBusOrderListActivity::class.java)
    }




    /**
     * 点击登录时销毁数据
     */
    @SuppressLint("CommitPrefEdits")
    private fun removeUserInfo() {
        // 当点击退出是提示是否要退出？
        AlertDialog.Builder(context).apply {
            setTitle("提示")
            setMessage("确定要退出登录吗!")
            setPositiveButton("取消") {_, _ -> }
            setNegativeButton("退出") {_, _ ->
                // 删除我们登录的时候存储的token和登录状态
                context?.getSharedPreferences("token", Context.MODE_PRIVATE)?.edit().apply {
                    // 将登录状态改为false
                    this?.putBoolean("user", false)
                    // 将token设置为空的字符串
                    this?.putString("token", "")
                    // 提交
                    this?.apply()
                }
                // 是否删除失败就不用写了
                // 直接调用onStart
                onStart()
            }
            show()
        }
    }



    /**
     * 获取用户基本信息
     */
    private suspend fun initUserInfo(token: String) {

        // 判断token是否为空
        if (token.isEmpty()) {
            "加载用户信息失败!".show()
            return
        }

        // 开启一个协程获取用户基本信息
        val result = api.getUserInfo(token)
        // 判断是否获取数据成功
        if (result.code == 200) activity?.runOnUiThread {

            // 将获取到的数据显示到屏幕上
            result.user.apply {
                // 显示用户昵称
                binding.personName.text = this?.nickName
                // 显示用户头像
                // 显示头像的URL地址端口不一样, 能显示头像的端口是10020
                val urlImage = "http://124.93.196.45:10002/"
                // 将URL地址和我们获取到的用户头像链接并接起来
                glide(urlImage + this?.avatar, binding.personImage)
            }
        }else activity?.runOnUiThread {
            result.msg?.show()
        }

    }


    /**
     * 验证用户是否登录
     * @param return 返回用户token
     */
    private fun validation(): String {
        val sh = activity?.getSharedPreferences("token", Context.MODE_PRIVATE)

        // 获取token
        val token = sh?.getString("token", "")

        // 判断用户登录状态
        if (sh?.getBoolean("user", true) == true) {
            // 如果为true侧用户已经登录， 我们更改按钮Text为 ‘退出’
            activity?.runOnUiThread {
                binding.outInfoTv.text = BTN_TEXT_2
            }
        } else if (sh?.getBoolean("user", false) == false) {
            activity?.runOnUiThread {
                // 如果没有登录， 侧显示按钮Text为 ‘登录’
                binding.outInfoTv.text = BTN_TEXT_1
            }
        }


        return token.toString()
    }


    /**
     * Fragment启动时，判断用户是否已经登录， 并获取用户信息
     */
    override fun onStart() {
        super.onStart()
        // 验证登录
        val token = validation()

        coroutine(Dispatchers.Main) {
            if (token != null) initUserInfo(token)
            else activity?.runOnUiThread {
                "加载用户数据失败!".show()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}