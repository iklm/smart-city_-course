package com.example.smartcity.ui.home.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smartcity.R
import com.example.smartcity.databinding.FragmentShowNewsBinding
import com.example.smartcity.logic.Repository.api
import com.example.smartcity.logic.Repository.coroutine
import com.example.smartcity.logic.model.HotNewsModel
import com.example.smartcity.logic.util.Util.show
import com.example.smartcity.ui.home.adapter.HomeNewsRecyclerAdapter

/**
 * 显示新闻列表
 */
// 定义一个 ShowNewsFragment 类，继承自 Fragment 类
class ShowNewsFragment : Fragment() {

    // 声明 FragmentShowNewsBinding 类型的变量
    private lateinit var mBinding: FragmentShowNewsBinding

    // 实现 onCreateView 方法，用于创建视图并返回该视图
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // 加载指定布局文件，并初始化 mBinding 变量
        mBinding = FragmentShowNewsBinding.inflate(layoutInflater, container, false)
        return mBinding.root // 返回加载后的视图对象
    }

    // 实现 onViewCreated 方法，当视图创建完成后调用
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        coroutine {
            getNews(showNewsId) // 异步获取新闻数据
        }

    }

    // 声明一个 HotNewsModel.RowsDTO 的可变数组变量 arrayNews
    var arrayNews = ArrayList<HotNewsModel.RowsDTO>()

    // 声明一个挂起函数 getNews，用于异步获取新闻列表数据
    private suspend fun getNews(id: Int){
        val result = api.getNewsAll(id) // 调用接口获取数据
        if (result.code == 200){ // 如果返回码为 200，则获取数据成功
            arrayNews.clear() // 清空原有数据
            arrayNews.addAll(result.rows!!) // 将新闻列表数据添加到 arrayNews 数组中

            activity?.runOnUiThread {
                // 设置 RecyclerView 的相关属性，并使用 HomeNewsRecyclerAdapter 适配器填充数据
                mBinding.showNewsRecyclerView.apply {
                    this.layoutManager = LinearLayoutManager(context) // 设置布局管理器为线性列表布局
                    this.isNestedScrollingEnabled = false // 禁止嵌套滚动
                    this.adapter = HomeNewsRecyclerAdapter(arrayNews, R.layout.re_show_news) // 设置适配器，使用 HomeNewsRecyclerAdapter 类，并传入 arrayNews 数组和布局资源 ID
                    this.adapter?.notifyItemChanged(arrayNews.size) // 更新 RecyclerView 的数据
                }
            }

        }else { // 如果返回码不是 200，则获取数据失败
            activity?.runOnUiThread {
                "加载新闻列表失败！".show() // 在 UI 线程上弹出提示信息
            }
        }
    }

    companion object{
        /**
         * companion object，它可以用于在类中创建静态变量和方法。在该代码段中，声明了一个名为 showNewsId 的静态变量，用于记录当前用户选择的新闻分类ID。

        当用户滑动 ViewPager2 或点击 TabLayout 时，我们可以根据不同的分类 ID 获取对应的新闻列表，并展示到 UI 上。例如，当用户选择“科技”分类时，我们可以将 showNewsId 更新为“科技”分类的 ID（比如 1），然后使用该 ID 来请求服务端接口获取“科技”分类下的新闻列表数据并展示出来。

        由于该变量是静态变量，所以可以在应用程序的任何位置使用，并且不需要创建该类的实例即可访问它。
         */
        var showNewsId = 0
    }

}