package com.example.smartcity.ui.AllService.SmartBus.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.example.smartcity.databinding.ActivitySmartBusPcactivityBinding
import com.example.smartcity.logic.Repository
import com.example.smartcity.logic.Repository.coroutine
import com.example.smartcity.logic.Repository.sApi
import com.example.smartcity.logic.util.Util.show
import kotlinx.coroutines.Dispatchers


/**
 * 智慧巴士 - 第三页， 显示 起点 - 终点的路线， 收集用户基本信息
 */
class SmartBusPCActivity : AppCompatActivity() {

    // 路线ID
    private val lineID by lazy { intent.getStringExtra("lineID") }

    // 日期信息
    private val date by lazy { intent.getStringExtra("date") }

    lateinit var mBinding: ActivitySmartBusPcactivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivitySmartBusPcactivityBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        // 设置标题
        title = "填写信息"
        // 显示回退按钮
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // 把当前意图添加到数组中
        Repository.smartAddActivity(this)

        // 输出信息
        // "路线ID: $lineID 日期: $date".show()

        // 获取线路信息
        getLineMsg()
        // 输入信息
        inputContent()
    }

    /**
     * 输入基本信息
     * @param edName 用户名称
     * @param edPhone 用户联系电话
     * @param edStartPlace 上车地点
     * @param edEndPlace 下车地点
     */
    private fun inputContent() {
        // 监听按钮点击事件
        mBinding.edBtn.setOnClickListener {
            // 先判断是否输入信息
            if (mBinding.edName.text.toString().isNotEmpty() &&
                mBinding.edPhone.text.toString().isNotEmpty() &&
                mBinding.edStartPlace.text.toString().isNotEmpty() &&
                mBinding.edEndPlace.text.toString().isNotEmpty()
                ) {

                // 到这里我们直接选择跳转到下一个Activity。
                startActivity(Intent(this, SmartBusShowContentActivity::class.java).apply {
                    // 将数据传过去
                    putExtra("lineID", lineID.toString()) // 路线ID
                    putExtra("date", date.toString()) // 日期信息
                    putExtra("edName", mBinding.edName.text.toString()) // 用户姓名
                    putExtra("edPhone", mBinding.edPhone.text.toString()) // 联系电话
                    putExtra("edStartPlace", mBinding.edStartPlace.text.toString()) // 上车地点
                    putExtra("edEndPlace", mBinding.edEndPlace.text.toString()) // 下车地点
                })

            } else runOnUiThread {
                // 没有输入信息提示用户
                "请输入基本信息".show()
            }
        }

    }



    /**
     * 获取线路信息
     */
    private fun getLineMsg() = coroutine(Dispatchers.Main) {
        // 发起网络请求
        sApi.getLineMsg(lineID!!).apply {
            // 判断是否获取成功
            if (this.code == 200) {
                // 填充信息
                runOnUiThread {
                    mBinding.tvStartOnEnd.text = "${this.data?.first} ----> ${this.data?.end}"
                }
            } else runOnUiThread {
                // 打印失败信息
                this.msg?.show()
            }
        }
    }


    /**
     * 点击回退按钮时销毁该页面
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}