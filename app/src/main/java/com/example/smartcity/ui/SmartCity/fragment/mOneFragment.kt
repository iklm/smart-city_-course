package com.example.smartcity.ui.SmartCity.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smartcity.R
import com.example.smartcity.databinding.FragmentMOneBinding
import com.example.smartcity.ui.SmartCity.adapter.mOneRvAdapter
import com.example.smartcity.ui.SmartCity.model.mOneMoodel

private val s = "开发ER系统， 熟悉JavaWeb， Spring boot等等...".repeat(2)

/**
 * 中国智造-厂商招聘-厂商招聘列表
 */
class mOneFragment : Fragment() {

    // 准备数据
    companion object {
        val mOneAllData = ArrayList<mOneMoodel>().apply {
            this.add(mOneMoodel("Java开发工程师", "13-20K", s))
            this.add(mOneMoodel("Java开发工程师", "13-20K", s))
            this.add(mOneMoodel("Java开发工程师", "13-20K", s))
            this.add(mOneMoodel("Java开发工程师", "13-20K", s))
            this.add(mOneMoodel("Java开发工程师", "13-20K", s))
            this.add(mOneMoodel("Java开发工程师", "13-20K", s))
            this.add(mOneMoodel("Java开发工程师", "13-20K", s))
            this.add(mOneMoodel("Java开发工程师", "13-20K", s))
        }
    }

    lateinit var mBinding: FragmentMOneBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = FragmentMOneBinding.inflate(layoutInflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showData()
    }

    private fun showData() {
        mBinding.mOneRecyclerView.apply {
            this.isNestedScrollingEnabled = false
            this.layoutManager = LinearLayoutManager(context)
            this.adapter = mOneRvAdapter(mOneAllData, R.layout.r_chinabuild_one_item)
            this.adapter?.notifyItemChanged(mOneAllData.size)
        }
    }
}