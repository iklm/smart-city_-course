package com.example.smartcity.ui.SmartCity.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.smartcity.databinding.ReRfItemBinding
import com.example.smartcity.logic.base.BaseRecyclerAdapter
import com.example.smartcity.ui.SmartCity.Activity.ContentShowActivity
import com.example.smartcity.ui.SmartCity.model.RFModel

/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/9/6 20:22
 *  created: Ikun
 *  Email: 2506789532@qq.com
 *
 *  中国智造-厂商推荐列表适配器
 **/
class RFAdapter(
    // 数据源
    private val list: MutableList<RFModel>,
    // 布局ID
    private val layoutID: Int
) : BaseRecyclerAdapter<RFModel, ReRfItemBinding>(list, layoutID){

    // 数据填充函数
    override fun setData(
        mBinding: ReRfItemBinding,
        data: RFModel,
        position: Int,
        holder: ViewHolder
    ) {
        val d = list[position]
        // 填充图片
        mBinding.rfImage.setImageResource(d.rfImage)
        // 填充标题
        mBinding.rfTitle.text = d.rfTitle
        // 填充介绍内容
        mBinding.rfContent.text = d.rfContent

        // 点击事件
        // 点击事跳转到下一个Activity显示内容， 我们将标题， 照片， 内容传入过去。
        holder.itemView.apply {
            val context = this.context
            this.setOnClickListener {
                context.apply {
                    this.startActivity(Intent(this, ContentShowActivity::class.java).apply {
                        // 将数据传入过去
                        putExtra("title", mBinding.rfTitle.text)
                        putExtra("img", d.rfImage.toString())
                        putExtra("content", mBinding.rfContent.text)
                    })
                }
            }
        }

    }


    override fun getInflate(layoutInflater: LayoutInflater, parent: ViewGroup): ReRfItemBinding {
        // 绑定视图
        return ReRfItemBinding.inflate(layoutInflater, parent, false)
    }

}