package com.example.smartcity.ui.SmartCity.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smartcity.R
import com.example.smartcity.databinding.FragmentMTwoBinding
import com.example.smartcity.ui.SmartCity.adapter.RFAdapter


/**
 * 中国智造-厂商招聘-校企合作列表
 */
class mTwoFragment : Fragment() {

    lateinit var mBinding: FragmentMTwoBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = FragmentMTwoBinding.inflate(layoutInflater, container,false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding.mTwoRecyclerView.apply {
            // 设置 isNestedScrollingEnabled 属性为 false，确保滑动性能良好
            this.isNestedScrollingEnabled = true
            // 设置 layoutManager 为 LineLayoutManager，以列表布局方式显示子项视图
            this.layoutManager = LinearLayoutManager(context)
            // 设置适配器
            this.adapter = RFAdapter(recommendFirmFragment.RFData, R.layout.re_rf_item)
            // 更新列表适配器
            this.adapter?.notifyItemChanged(recommendFirmFragment.RFData.size)
        }
    }
}