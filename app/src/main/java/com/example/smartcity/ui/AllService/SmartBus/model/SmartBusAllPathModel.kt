package com.example.smartcity.ui.AllService.SmartBus.model

/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/9/22 15:02
 *  created: Ikun
 *  Email: 2506789532@qq.com
 *
 *  智慧巴士 - 查看路线实体类
 *  @see com.example.smartcity.ui.AllService.SmartBus.adapter.SmartBusAllPathAdapter
 **/
data class SmartBusAllPathModel (val title: String)