package com.example.smartcity.ui.SmartCity.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smartcity.R
import com.example.smartcity.databinding.FragmentProductBinding
import com.example.smartcity.ui.SmartCity.adapter.RFAdapter
import com.example.smartcity.ui.SmartCity.model.RFModel

/**
 * 产品与解决方案
 */
class productFragment : Fragment() {

    // 产品数据
    companion object {

        // repeat将该字符串重复拼接100次
        val allData = ArrayList<RFModel>().apply {
            this.add(RFModel(R.mipmap.a1, "Apple1", "Apple1".repeat(100)))
            this.add(RFModel(R.mipmap.a1, "Apple2", "Apple2".repeat(100)))
            this.add(RFModel(R.mipmap.a1, "Apple3", "Apple3".repeat(100)))
            this.add(RFModel(R.mipmap.a1, "Apple4", "Apple4".repeat(100)))
            this.add(RFModel(R.mipmap.a1, "Apple5", "Apple5".repeat(100)))
            this.add(RFModel(R.mipmap.a1, "Apple1", "Apple1".repeat(100)))
            this.add(RFModel(R.mipmap.a1, "Apple2", "Apple2".repeat(100)))
            this.add(RFModel(R.mipmap.a1, "Apple3", "Apple3".repeat(100)))
            this.add(RFModel(R.mipmap.a1, "Apple4", "Apple4".repeat(100)))
            this.add(RFModel(R.mipmap.a1, "Apple5", "Apple5".repeat(100)))
        }
    }

    lateinit var mBinding: FragmentProductBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        mBinding = FragmentProductBinding.inflate(layoutInflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pDataShow()
    }

    /**
     * RecyclerView适配器
     */
    private fun pDataShow() {
        mBinding.pRecyclerView.apply {
            this.isNestedScrollingEnabled = false
            // 使用垂直布局显示item布局
            this.layoutManager = LinearLayoutManager(context)
            // 设置适配器
            this.adapter = RFAdapter(allData, R.layout.re_rf_item)
            // 刷新数据
            this.adapter?.notifyItemChanged(allData.size)
        }
    }
}







































