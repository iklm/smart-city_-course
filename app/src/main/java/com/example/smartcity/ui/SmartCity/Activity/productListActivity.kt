package com.example.smartcity.ui.SmartCity.Activity

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smartcity.R
import com.example.smartcity.databinding.ActivityProductListBinding
import com.example.smartcity.ui.SmartCity.adapter.ProductListAdapter
import com.example.smartcity.ui.SmartCity.model.ProductListModel


/**
 * 中国智造-产品列表模块
 */
class productListActivity : AppCompatActivity() {

    lateinit var mBinding: ActivityProductListBinding

    companion object {
         val allData = ArrayList<ProductListModel>().apply {
             this.add(ProductListModel(R.mipmap.img1, "科技1"))
             this.add(ProductListModel(R.mipmap.img2, "科技2"))
             this.add(ProductListModel(R.mipmap.img1, "科技3"))
             this.add(ProductListModel(R.mipmap.img2, "科技4"))
             this.add(ProductListModel(R.mipmap.img1, "科技5"))
             this.add(ProductListModel(R.mipmap.img2, "科技6"))
         }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityProductListBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        // 显示回退按钮
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        // 设置标题
        title = "产品列表"


        showProduct()
    }


    /**
     * 配置列表适配器
     */
    @SuppressLint("NotifyDataSetChanged")
    private fun showProduct() {
        // 获取产品列表的RecyclerView
        mBinding.pRecyclerView.apply {

            // 允许嵌套滚动
            this.isNestedScrollingEnabled = true

            // 设置布局管理器为网格布局，每行显示3个项目
            this.layoutManager = GridLayoutManager(context, 3)

            // 设置适配器为自定义的ProductListAdapter，传入数据和布局资源
            this.adapter = ProductListAdapter(allData, R.layout.re_chinabuild_productlist_item)

            // 通知适配器数据变化
            this.adapter?.notifyDataSetChanged()
        }
    }


    /**
     * 当点击回退按钮时， 销毁该界面
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}