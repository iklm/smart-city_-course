package com.example.smartcity.logic.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.OpenForTesting
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.bumptech.glide.Glide
import com.example.smartcity.App
import com.example.smartcity.App.Companion.context
import com.example.smartcity.App.Companion.getToken
import com.example.smartcity.ui.notifications.activity.SmartBusOrderListActivity
import kotlin.reflect.KClass

/**
 *
 *  ANDROID STUDIO version 2022.1
 *  codeAuthor: ikun
 *  email/qq: 2506789532@qq.com
 *  createTime: 2023/5/6 23:29
 *  ------------------------------
 *
 **/
/**
 * 这个对象包含一些常用的实用函数。
 */
object Util {

    /**
     * 在当前上下文中显示一个Toast消息。
     *
     * @param context 消息应该显示在其中的上下文。
     * @param message 要显示的消息文本。
     */
    fun String.show() {
        Toast.makeText(context, this, Toast.LENGTH_SHORT).show()
    }

    /**
     * 使用Glide库加载指定URL的图像，并将其设置为给定ImageView的图像。
     *
     * @param imageUrl 图像的URL地址。
     * @param imageView 要设置图像的ImageView实例。
     */
    fun glide(imageUrl: String, imageView: ImageView) =
        Glide.with(context).load(imageUrl).into(imageView)


    /**
     * 这个只能用于个人中心的卡片布局哦！
     * 封装一个函数，用于在 CardView 被点击时判断用户是否已经登录，如果已登录则启动指定的 Activity，否则显示提示信息。
     *
     * @param context 上下文对象，用于启动 Activity 或显示提示信息
     * @param msg 提示内容，当用户未登录时显示
     * @param activity 启动意图对象，当用户已登录时启动的目标 Activity
     */
    fun CardView.onClick(context: Context, msg: String, activitys: Class<out Activity>) {
        // 这是给 CardView 设置点击监听器的语法。当 CardView 被点击时，会执行大括号内的代码块。
        this.setOnClickListener {
            // getToken 是否不为空，如果不为空则执行大括号内的代码块，否则执行 msg.show()。
            if (getToken.isNotEmpty()) {
                // 如果 getToken 不为空，就会调用 context 的 startActivity 方法，以启动一个新的 Activity。
                // 这个新的 Activity 是由传入的 activity 参数决定的，通过 Intent 来指定要启动的 Activity 类。
                context.startActivity(Intent(context, activitys))
            } else msg.show() // 如果 getToken 为空，就会调用 msg 的 show 方法，用于显示提示信息。
        }
    }



}