package com.example.smartcity.logic.model.SmartBusModel

import com.google.gson.annotations.SerializedName

/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/9/29 23:28
 *  created: Ikun
 *  Email: 2506789532@qq.com
 *
 *  智慧巴士 - 提交订单实体类
 **/
class SmartBusOrdersModel {
    @SerializedName("msg")
    var msg: String? = null

    @SerializedName("code")
    var code: Int? = null

    @SerializedName("orderNum")
    var orderNum: String? = null
}