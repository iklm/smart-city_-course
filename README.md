# 智慧城市Kotlin版 （更新中）
QQ: 2506789532  
WX: wjk655955

## 工具版本

Android studio: 2022(使用最新版本)

Gradle: 1.8



## 前言

本教程是根据我在比赛的时候，是如何一步一步的开发 智慧城市App 的

所需要的插件比赛会提供，以下是我在比赛时所用到的插件：

![image-20230506211806702](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506211806702.png)



现在我们开始进行开发吧！





## 创建项目

比赛时提供AS自带的Bottom Navigation的，我们直接使用AS自带的创建项目就好了，不用手写底部导航栏。

### Bottom Navigation

![image-20230506212017742](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506212017742.png)



### 项目名称

![image-20230506212121334](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506212121334.png)

点击完成就ok了





![image-20230506212233618](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506212233618.png)

等待下载依赖





### 配置字符串资源

![image-20230506212711425](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506212711425.png)



### 添加我们所需要的插件

![image-20230506212826950](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506212826950.png)



点击项目的build.gradle, 默认时只有*.jar, 我们手动添加 *.aar, 就可以加载带.aar的插件依赖了

![image-20230506212951242](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506212951242.png)



### 添加模块目录



![image-20230506213527740](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506213527740.png)

dashboard: 底部导航栏 - 全部服务

data: 底部导航栏 - 数据分析（根据比赛提供题目来设置）

home: 底部导航栏 - 主页

notifications: 底部导航栏 - 个人中心

SmartCity： 底部导航栏 - 中国智造(根据比赛提供题目来设置)







### 创建对应的Fragment

![image-20230506214116447](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506214116447.png)







### 添加Menu Item

![image-20230506214303412](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506214303412.png)







### navigation

点击navigation

![image-20230506214413802](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506214413802.png)

打开moblie_navigation.xml





![image-20230506214522296](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506214522296.png)

![image-20230506214607967](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506214607967.png)

![image-20230506214700084](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506214700084.png)







#### 设置Id

![image-20230506214914669](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506214914669.png)







#### MainActivity添加ID

![image-20230506215040393](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506215040393.png)



### 运行测试

![image-20230506220210555](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506220210555.png)

运行效果出来了，可是底部导航栏文字没显示全部怎么办？



#### 解决方法

![image-20230506220157572](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506220157572.png)



再次运行看看

![image-20230506220858884](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506220858884.png)



创建项目就到这里了。接下来我们开始封装RecyclerView和网络访问，还有Kotlin协程







## 封装RecyclerView和Retrofit

比赛我所用到的列表都是用RecyclerView来实现的

![image-20230506221531742](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506221531742.png)

在smartcity包下新建一个logic包，里面有 model。network。Repository

base: 封装BaseRecyclerAdapter对象

model: 实体类数据

network: ‘网络封装’ 和 ’服务器地址接口对象‘







### BaseRecyclerAdapter

在base包下新建BaseRecyclerAdapter抽象类对象

看代码注释

``` kotlin
abstract class BaseRecyclerAdapter <E, VB: ViewBinding>(
    private val list: MutableList<E>,
    @LayoutRes
    private val layoutId: Int,
) : RecyclerView.Adapter<BaseRecyclerAdapter<E, VB>.ViewHolder>(){

    // 填充数据
    abstract fun setData(mBinding: VB, data: E, position: Int, holder: ViewHolder)

    // 获取布局
    abstract fun getInflate(layoutInflater: LayoutInflater, parent: ViewGroup) : VB


    override fun onBindViewHolder(holder: ViewHolder, position: Int)
            = holder.bindData(list[position], position)

    // 列表数量
    override fun getItemCount(): Int = list.size

    // 布局Id
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
            = ViewHolder(getInflate(LayoutInflater.from(parent.context), parent))

    inner class ViewHolder(private val viewBinding: VB) : RecyclerView.ViewHolder(viewBinding.root) {
        fun bindData(data: E, position: Int){
            setData(viewBinding, data, position, this)
        }
    }
}
```

到这里RecyclerView就封装完了。接下来我们封装网络访问Retrofit





### 封装Retrofit

在network包下新建NetService

这个对象用于创建Retrofit实例，其中包含一个Http请求的基本URL和Gson转换器。

```kotlin
/**
 * 这个对象用于创建Retrofit实例，其中包含一个Http请求的基本URL和Gson转换器。
 */
object NetService {
    // Http请求的基本URL
    private const val BASE_URL = "服务器地址"

    // 创建Retrofit实例
    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    /**
     * 用于创建给定服务类的实例。
     *
     * @param serviceClass 要创建的服务类。
     * @return 给定服务类的实例（使用Retrofit创建）。
     */
    fun <E> create(serviceClass: Class<E>) : E = retrofit.create(serviceClass)

    /**
     * 在不指定服务类的情况下，使用内联函数来创建服务类的实例。
     *
     * @param R 要创建实例的服务类。
     * @return 指定服务类的实例（使用Retrofit创建）。
     */
    inline fun <reified R> create() : R = create(R::class.java)
}
```



### 接口

接着在network包下新建SmartApi接口对象

```kotlin
/**
 * 这个接口定义了一些用于从服务器获取数据的网络请求方法。
 */
interface SmartApi {
    /**
     * 使用GET请求获取banner数据。
     *
     * @param map 包含查询参数的Map对象，其中键表示参数名称，值表示参数值。
     * @return 与HTTP请求响应对应的BannerModel实例。该方法使用suspend关键字来指示它是一个挂起函数，
     *         这意味着在该函数执行期间，调用者协程将被暂停，直到此网络请求完成为止。
     */
    @GET("")
    suspend fun getBanner(@QueryMap map: Map<String, Int>) : BannerModel
}
```



挂起函数返回的BannerModel是什么呢？

这个BannerModel是服务器返回Json数据的实体类对象，需要将json返回的数据转换成kotlin数据类

但是比赛提供的插件不支持将Json转换成kotlin的数据类对象，那怎么办呢！

别急，有办法的。我们可以先将服务器返回的Json数据转换成Java实体类对象，再将java实体类对象的代码复制到我们创建好的Kotlin对象里面粘贴，这时候AS工具会提示是否将Java代码转换成Kotlin代码，我们只需要点击ok就可以了。

接下来看我怎么操作吧。



#### 导入插件

将Json数据转换成Java实体类需要使用插件，刚好比赛时提供将Json数据转换成Java实体类对象的

![image-20230506224453143](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506224453143.png)

![image-20230506224557468](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506224557468.png)



在AS里面打开Fiel>settings

![image-20230506224719169](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506224719169.png)

![image-20230506224752672](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506224752672.png)

找到我们存放的插件包路径，点击第一个，然后点击ok就可以了



我们需要重启一下AS

![image-20230506224927047](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506224927047.png)

![image-20230506224942985](C:\Users\ikun\AppData\Roaming\Typora\typora-user-images\image-20230506224942985.png)