package com.example.smartcity.ui.AllService.SmartBus.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smartcity.App.Companion.context
import com.example.smartcity.R
import com.example.smartcity.databinding.RSmbsHomeItemBinding
import com.example.smartcity.logic.Repository.coroutine
import com.example.smartcity.logic.Repository.sApi
import com.example.smartcity.logic.base.BaseRecyclerAdapter
import com.example.smartcity.logic.model.SmartBusModel.SmartBusSiteModel
import com.example.smartcity.ui.AllService.SmartBus.activity.SmartBusDateActivity
import com.example.smartcity.ui.AllService.SmartBus.model.SmartBusAllLineModel
import com.example.smartcity.ui.AllService.SmartBus.model.SmartBusAllPathModel
import kotlinx.coroutines.Dispatchers

/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/9/19 23:20
 *  created: Ikun
 *  Email: 2506789532@qq.com
 *
 *  智慧巴士 - 列表适配器
 **/
class SmartBusAllLineRvAdapter(
    // 数据源
    private val list: MutableList<SmartBusAllLineModel>,  // 智慧巴士线路列表数据
    private val layoutID: Int  // 布局资源ID
) : BaseRecyclerAdapter<SmartBusAllLineModel, RSmbsHomeItemBinding>(list, layoutID)
{
    /**
     * 将数据设置到视图中
     *
     * @param mBinding 数据绑定对象
     * @param data 数据对象
     * @param position 数据在列表中的位置
     * @param holder ViewHolder
     */
    override fun setData(
        mBinding: RSmbsHomeItemBinding,
        data: SmartBusAllLineModel,
        position: Int,
        holder: ViewHolder
    ) {

        // 获取数据
        val result = list[position].apply {
            // 填充数据到UI上
            mBinding.smbsLineName.text = this.lineName      // 设置线路名称
            mBinding.smbsLineStart.text = this.lineStart    // 设置起点站
            mBinding.smbsLineEnd.text = this.lineEnd        // 设置终点站
            mBinding.smbsLineStartTime.text = "首发时间: ${this.lineStartTime}" // 设置首发时间
            mBinding.smbsLineEndTime.text = "末班时间: ${this.lineEndTime}"     // 设置末班时间

            // 传入路线ID
            getSiteContent(this.lineID, mBinding)

            var ctx = holder.itemView.context

            // 状态
            var state = false
            // 点击查看更多路线
            mBinding.smbsAllLineTv.setOnClickListener {
                if (state) {
                    // 更改状态
                    state = false
                    // 将布局隐藏
                    mBinding.smbsShowLineRecyclerView.visibility = View.GONE
                    // 调用刚刚封装好的代码
                    drawableEnd(true, "查看路线", mBinding)
                } else {
                    // 更改状态
                    state = true
                    // 将布局显示出来
                    mBinding.smbsShowLineRecyclerView.visibility = View.VISIBLE
                    // 调用刚刚封装好的代码
                    drawableEnd(false, "隐藏路线", mBinding)
                }
            }


        }

        /**
         *  点击跳转到下一页面， 选择出行日期
         *
         *  // 跳转目标
         *  @see com.example.smartcity.ui.AllService.SmartBus.activity.SmartBusDateActivity
         *
         *
         */
        holder.itemView.apply {
            val ctx = this.context
            // 监听点击事件
            this.setOnClickListener {
                // 跳转
                ctx.startActivity(Intent(ctx, SmartBusDateActivity::class.java).apply {
                    // 传入我们点击的item列表路线的id
                    putExtra("id", result.lineID.toString())
                })
            }
        }

    }

    /**
     * 发起网络请求获取站点信息
     *
     * @param id 路线ID
     * @param mBinding 数据绑定对象
     */
    private fun getSiteContent(id: Int, mBinding: RSmbsHomeItemBinding) = coroutine(Dispatchers.Main) {
        // 存储站点数据
        val siteData = ArrayList<SmartBusAllPathModel>()

        // 发起网络请求
        var result = sApi.getSite(id)

        // 判断是否获取成功
        if (result.code == 200) {
            // 遍历获取数据
            for (i in result.rows!!) {
                // 添加数据
                siteData.add(SmartBusAllPathModel(i.name.toString()))
            }

            // 遍历存储数据后，配置列表适配器
            mBinding.smbsAllLineRecyclerView.apply {
                // 设置是否允许嵌套滚动
                this.isNestedScrollingEnabled = false

                // 设置布局管理器为水平方向的LinearLayoutManager
                this.layoutManager = LinearLayoutManager(context).apply {
                    this.orientation = LinearLayoutManager.HORIZONTAL
                }

                // 设置适配器为 SmartBusAllPathAdapter，并传入数据源 siteData 和布局资源文件 R.layout.re_bus_home_all
                this.adapter = SmartBusAllPathAdapter(siteData, R.layout.re_bus_home_all)

                // 通知适配器数据发生变化
                this.adapter?.notifyItemChanged(siteData.size)
            }
        }
    }

    /**
     * 封装更改DrawableEnd的方法
     *
     * @param state 布尔值，true表示显示“查看路线”，false表示显示“隐藏路线”
     * @param title 显示的文本
     * @param tv 数据绑定对象
     */
    fun drawableEnd(state: Boolean, title: String, tv: RSmbsHomeItemBinding) {
        tv.smbsAllLineTv.apply {
            when(state) {
                true -> {
                    // 更改文本
                    this.text = title
                    // 更改图标
                    this.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        null, null,
                        resources.getDrawable(R.drawable.smbs_bottom), null
                    )
                }

                false -> {
                    // 更改文本
                    this.text = title
                    // 更改图标
                    this.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        null, null,
                        resources.getDrawable(R.drawable.smbs_top), null
                    )
                }
            }
        }
    }

    /**
     * 创建并返回数据绑定对象
     *
     * @param layoutInflater 布局加载器
     * @param parent 父ViewGroup
     * @return RSmbsHomeItemBinding 返回数据绑定对象
     */
    override fun getInflate(
        layoutInflater: LayoutInflater,
        parent: ViewGroup
    ): RSmbsHomeItemBinding {
        return RSmbsHomeItemBinding.inflate(layoutInflater, parent, false)
    }
}
