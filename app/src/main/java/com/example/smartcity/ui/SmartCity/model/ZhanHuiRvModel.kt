package com.example.smartcity.ui.SmartCity.model

/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/9/14 23:05
 *  created: Ikun
 *  Email: 2506789532@qq.com
 *
 *  中国智造主页-展会活动-实体类
 **/
data class ZhanHuiRvModel (
    val zImg: Int, // 图片资源id
    val zTitle: String // 标题/内容
)