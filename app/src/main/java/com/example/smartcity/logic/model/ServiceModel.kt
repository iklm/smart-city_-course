package com.example.smartcity.logic.model

import com.google.gson.annotations.SerializedName




/**
 *
 *  ANDROID STUDIO version 2022.1
 *  codeAuthor: ikun
 *  email/qq: 2506789532@qq.com
 *  createTime: 2023/5/7 12:11
 *  ------------------------------
 *
 **/
class ServiceModel {
    @SerializedName("code")
     val code: Int? = null

    @SerializedName("msg")
     val msg: String? = null

    @SerializedName("rows")
     val rows: List<RowsDTO>? = null

    @SerializedName("total")
     val total: Int? = null

    class RowsDTO {
        @SerializedName("id")
         val id: Int? = null

        @SerializedName("serviceName")
         val serviceName: String? = null

        @SerializedName("serviceDesc")
         val serviceDesc: String? = null

        @SerializedName("serviceType")
         val serviceType: String? = null

        @SerializedName("imgUrl")
         val imgUrl: String? = null

        @SerializedName("link")
         val link: String? = null

        @SerializedName("sort")
         val sort: Int? = null

        @SerializedName("isRecommend")
         val isRecommend: String? = null
    }
}