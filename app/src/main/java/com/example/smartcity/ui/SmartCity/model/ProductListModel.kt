package com.example.smartcity.ui.SmartCity.model

/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/9/13 17:57
 *  created: Ikun
 *  Email: 2506789532@qq.com
 *
 *  中国智造主页-产品列表模块实体类
 **/
data class ProductListModel (
    val pImg: Int,
    val pTitle: String
)