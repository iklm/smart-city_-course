package com.example.smartcity.ui.SmartCity.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.smartcity.databinding.ReChinabuildItemBinding
import com.example.smartcity.logic.base.BaseRecyclerAdapter
import com.example.smartcity.ui.SmartCity.Activity.ManuFacturerActivity
import com.example.smartcity.ui.SmartCity.Activity.MsiActivity
import com.example.smartcity.ui.SmartCity.Activity.ZhanHuiActivity
import com.example.smartcity.ui.SmartCity.Activity.productListActivity
import com.example.smartcity.ui.SmartCity.model.ChinaBuildModel

/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/9/6 16:07
 *  created: Ikun
 *  Email: 2506789532@qq.com
 **/
/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/9/6 16:07
 *  created: Ikun
 *  Email: 2506789532@qq.com
 **/
class ChinaBuildMoreAdapter(
    // 数据源
    private val list: MutableList<ChinaBuildModel>,
    // 布局ID
    private val layoutID: Int
) : BaseRecyclerAdapter<ChinaBuildModel, ReChinabuildItemBinding>(list, layoutID) {

    // setData 方法用于设置每个子项视图中 UI 控件的数据
    override fun setData(
        mBinding: ReChinabuildItemBinding,
        data: ChinaBuildModel,
        position: Int,
        holder: ViewHolder
    ) {

        // 获取当前数据项
        val d = list[position]

        // 填充数据
        mBinding.cMoreImage.setImageResource(d.img) // 图标
        mBinding.cMoreTitle.text = d.title // 标题

        // 点击事件
        holder.itemView.apply {
            val context = this.context
            this.setOnClickListener {

                // 点击产品列表小模块跳转过去
                if (d.title == "产品列表") {
                    context.startActivity(Intent(context, productListActivity::class.java))
                }

                // 点击跳转展会活动
                if (d.title == "展会活动") {
                    context.startActivity(Intent(context, ZhanHuiActivity::class.java))
                }

                // 点击跳转到厂商招聘
                if (d.title == "厂商招聘") {
                    context.startActivity(Intent(context, ManuFacturerActivity::class.java))
                }

                // 点击跳转到厂商入驻页面
                if (d.title == "厂商入驻") {
                    context.startActivity(Intent(context, MsiActivity::class.java))
                }
            }
        }

    }

    // getInflate 方法负责将布局文件转换为视图绑定对象
    override fun getInflate(
        layoutInflater: LayoutInflater,
        parent: ViewGroup
    ): ReChinabuildItemBinding {
        // 使用 inflate 方法将布局文件转换为视图绑定对象
        return ReChinabuildItemBinding.inflate(layoutInflater, parent, false)
    }

}










