package com.example.smartcity.ui.SmartCity.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smartcity.R
import com.example.smartcity.databinding.ActivityProductListShowBinding
import com.example.smartcity.ui.SmartCity.adapter.RFAdapter
import com.example.smartcity.ui.SmartCity.fragment.productFragment


/**
 * 中国智造-产品列表模块-产品列表展示
 */
class ProductListShowActivity : AppCompatActivity() {

    lateinit var mBinding: ActivityProductListShowBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityProductListShowBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        // 显示回退按钮
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        // 设置标题
        title = "产品列表"


        // 显示列表数据
        showListAdapter()
    }


    /**
     * 列表适配器
     */
    private fun showListAdapter() {
        mBinding.pShowListRecyclerView.apply {
            this.isNestedScrollingEnabled = false
            // 使用垂直布局显示item布局
            this.layoutManager = LinearLayoutManager(context)
            // 设置适配器
            this.adapter = RFAdapter(productFragment.allData, R.layout.re_rf_item)
            // 刷新数据
            this.adapter?.notifyItemChanged(productFragment.allData.size)
        }
    }


    /**
     * 当点击回退按钮时销毁该界面
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}