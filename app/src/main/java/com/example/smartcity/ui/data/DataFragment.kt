package com.example.smartcity.ui.data

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.smartcity.R
import com.example.smartcity.databinding.FragmentDashboardBinding
import com.example.smartcity.databinding.FragmentDataBinding
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet

/**
 * 数据分析
 */
class DataFragment : Fragment() {


    private lateinit var mBinding: FragmentDataBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentDataBinding.inflate(inflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        barChartTest()
        lineChart()
    }


    /**
     * 柱状图
     */
    private fun barChartTest() {
        // 创建一个泛型数组存储数据
        val bData = ArrayList<BarEntry>().apply {
            for (i in 1..7 ) {
                // 生成随机数据
                val num = (99..3000).random()
                // 添加到柱状图数据中
                this.add(BarEntry(i.toFloat(), num.toFloat()))
            }
        }

        val dataSet = BarDataSet(bData, "客运周转量")
        val addData = ArrayList<IBarDataSet>()
        addData.add(dataSet)
        val dataBar = BarData(dataSet)
        mBinding.dataBarChartTest.apply {
            this.data = dataBar

            // 设置动画
            this.animateY(1500)
            this.animateX(2100)
            this.invalidate() // 显示出来
        }
    }


    /**
     * 折线图
     */
    private fun lineChart() {
        // 展示的数据
        val lineData = ArrayList<Entry>().apply {
            // 随机生成8条数据
            for (i in 0..7) {
                val num = (1..5000).random()
                this.add(Entry(i.toFloat(), num.toFloat()))
            }
        }

        val lineDataSet = LineDataSet(lineData, "客流量")
        val lineData1 = LineData(lineDataSet)
        mBinding.dataLineChart.data = lineData1

        // 设置坐标文本显示
        mBinding.dataLineChart.xAxis.valueFormatter = object : IndexAxisValueFormatter() {
            val str = listOf<String>(
                "10-22",
                "10-23",
                "10-24",
                "10-25",
                "10-26",
                "10-27",
                "10-28",
                "10-29",
            )

            override fun getAxisLabel(value: Float, axis: AxisBase?): String {
                return str[value.toInt()]
            }
        }

        // 设置动画
        mBinding.dataLineChart.animateX(1000)
        mBinding.dataLineChart.animateY(1500)

    }


}