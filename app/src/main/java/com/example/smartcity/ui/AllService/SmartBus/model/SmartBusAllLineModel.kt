package com.example.smartcity.ui.AllService.SmartBus.model

/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/9/19 23:12
 *  created: Ikun
 *  Email: 2506789532@qq.com
 *
 *  智慧巴士 - 线路列表实体类
 *
 *  @param lineID 路线ID
 *  @param lineName 线路名称
 *  @param lineRoute 线路里程
 *  @param lineStart 起点站
 *  @param lineEnd 终点站
 *  @param lineStartTime 首发时间
 *  @param lineEndTime 末班时间
 **/
data class SmartBusAllLineModel(
    val lineID: Int, // 路线ID
    val lineName: String,        // 线路名称
    val lineRoute: String,      // 线路里程
    val lineStart: String,      // 起点站
    val lineEnd: String,        // 终点站
    val lineStartTime: String,  // 首发时间
    val lineEndTime: String     // 末班时间
)
