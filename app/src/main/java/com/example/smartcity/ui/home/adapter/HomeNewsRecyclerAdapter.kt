package com.example.smartcity.ui.home.adapter

import android.content.Intent
import android.gesture.Gesture
import android.text.Html
import android.util.Log
import android.view.GestureDetector
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.ViewGroup
import com.example.smartcity.App.Companion.url
import com.example.smartcity.databinding.ReShowNewsBinding
import com.example.smartcity.logic.base.BaseRecyclerAdapter
import com.example.smartcity.logic.model.HotNewsModel
import com.example.smartcity.logic.util.Util.glide
import com.example.smartcity.logic.util.Util.show
import com.example.smartcity.ui.home.News.ShowNewsInfoActivity

/**
 *
 *  ANDROID STUDIO version 2022.1
 *  codeAuthor: ikun
 *  email/qq: 2506789532@qq.com
 *  createTime: 2023/5/7 23:59
 *  ------------------------------
 *  主页新闻专栏适配器
 **/
// 定义一个 HomeNewsRecyclerAdapter 类，继承自 BaseRecyclerAdapter 类，并设置泛型参数 HotNewsModel.RowsDTO 和 ReShowNewsBinding
class HomeNewsRecyclerAdapter(
    private val list: MutableList<HotNewsModel.RowsDTO>, // 传入数据列表
    private val layoutId: Int // 布局资源 ID
) : BaseRecyclerAdapter<HotNewsModel.RowsDTO, ReShowNewsBinding>(list, layoutId) {

    // 实现 setData 方法，用于填充数据到 RecyclerView 的 item 视图中
    override fun setData(
        mBinding: ReShowNewsBinding, // 绑定视图的布局文件
        data: HotNewsModel.RowsDTO, // 当前 item 对应的数据对象
        position: Int, // 当前 item 在列表中的位置
        holder: ViewHolder // ViewHolder 对象
    ) {
        // 从数据列表中获取当前位置的数据对象
        val d = list[position]

        // 填充数据
        glide(url + d.cover, mBinding.showNewsImageView) // 使用 Glide 加载图片数据到 ImageView 视图中
        mBinding.showNewsTitle.text = d.title // 设置新闻标题
        mBinding.showNewsContent.text = Html.fromHtml(d.content) // 设置新闻内容
        mBinding.showNewsData.text = "发布时间: ${d.publishDate}" // 设置新闻发布时间
        mBinding.showNewsUserSize.text = "${d.likeNum}评论" // 设置新闻评论数

        // 为 RecyclerView 中的列表项设置点击监听器
        holder.itemView.setOnClickListener {
            // 获取该列表项所在的上下文对象
            val con = holder.itemView.context
            // 启动 ShowNewsInfoActivity 并将该新闻的 id 以 "id" 为 key 进行传递
            con.startActivity(Intent(con, ShowNewsInfoActivity::class.java)
                .putExtra("id", d.id.toString()))
        }
    }

    // 实现 getInflate 方法，用于获取绑定视图的布局文件
    override fun getInflate(layoutInflater: LayoutInflater, parent: ViewGroup): ReShowNewsBinding {
        return ReShowNewsBinding.inflate(layoutInflater, parent, false) // 加载指定的布局文件，并返回对应的视图绑定对象
    }
}