package com.example.smartcity.logic.model

class LoginModel {
    val code: Int? = null
    val msg: String? = null
    val token: String? = null
}
