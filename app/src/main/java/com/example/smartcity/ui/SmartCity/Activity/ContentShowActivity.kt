package com.example.smartcity.ui.SmartCity.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import com.example.smartcity.R
import com.example.smartcity.databinding.ActivityContentShowBinding
import com.example.smartcity.logic.util.Util.show

/**
 * 中国智造主页列表-详情
 */
class ContentShowActivity : AppCompatActivity() {

    lateinit var mBinding: ActivityContentShowBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityContentShowBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        // 设置标题
        title = "详情"
        // 显示回退按钮
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        intent.apply {
            val title = this.getStringExtra("title")
            val img = this.getStringExtra("img")
            val content = this.getStringExtra("content")

            // 输出看看
//            Log.d("TestShowData", "onCreate: 标题${title}")
//            Log.d("TestShowData", "onCreate: 照片${img}")
//            Log.d("TestShowData", "onCreate: 内容${content}")

            // 设置标题
            mBinding.TvTitle.text = title
            // 设置图片
            img?.toInt()?.let { mBinding.contentImg.setImageResource(it) }
            // 设置发布时间 （随便设置）
            mBinding.TvTime.text = "2023-9-11"
            // 发布者 （随便设置）
            mBinding.TvUser.text = "智慧城市"
            // 设置第一个内容
            mBinding.TvContent1.text = content?.repeat(1)
            // 设置第二个内容
            mBinding.TvContent2.text = content?.repeat(20)
        }

        // 播放视频
        startVideo()
    }


    /**
     * 播放视频
     */
    private fun startVideo() = mBinding.contentVideoView.apply {
        this.width.inc()
        // 获取视频地址
        val videoUrl = "android.resource://$packageName/${R.raw.video2}"
        this.setVideoPath(videoUrl)
        this.start()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // 当点击回退按钮时， 销毁该界面
        finish()
        // 释放播放器
        mBinding.contentVideoView.suspend()
        return super.onOptionsItemSelected(item)
    }
}