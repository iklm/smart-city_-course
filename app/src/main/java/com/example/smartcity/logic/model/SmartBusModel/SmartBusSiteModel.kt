package com.example.smartcity.logic.model.SmartBusModel

import com.google.gson.annotations.SerializedName

/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/9/22 21:04
 *  created: Ikun
 *  Email: 2506789532@qq.com
 *
 *  智慧巴士 - 站点路线实体类
 *  @see com.example.smartcity.ui.AllService.SmartBus.adapter.SmartBusAllPathAdapter
 **/
class SmartBusSiteModel {
    @SerializedName("total")
    var total: Int? = null

    @SerializedName("rows")
    var rows: List<RowsDTO>? = null

    @SerializedName("code")
    var code: Int? = null

    @SerializedName("msg")
    var msg: String? = null

    class RowsDTO {
        @SerializedName("linesId")
        var linesId: Int? = null

        @SerializedName("stepsId")
        var stepsId: Int? = null

        @SerializedName("name")
        var name: String? = null

        @SerializedName("sequence")
        var sequence: Int? = null
    }
}