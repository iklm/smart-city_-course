package com.example.smartcity.ui.AllService.SmartBus

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smartcity.R
import com.example.smartcity.databinding.ActivitySmartBusBinding
import com.example.smartcity.logic.Repository.coroutine
import com.example.smartcity.logic.Repository.sApi
import com.example.smartcity.logic.util.Util.show
import com.example.smartcity.ui.AllService.SmartBus.adapter.SmartBusAllLineRvAdapter
import com.example.smartcity.ui.AllService.SmartBus.model.SmartBusAllLineModel
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.coroutineContext


/**
 * TODO 智慧巴士模块
 */
class SmartBusActivity : AppCompatActivity() {

    // 存储我们获取到的线路数据
    private var allLineData = ArrayList<SmartBusAllLineModel>()

    private lateinit var mBinding: ActivitySmartBusBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivitySmartBusBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        // 显示回退按钮
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        // 设置标题
        title = "智慧巴士"

        // 发起网络请求获取数据， 并配置列表适配器将数据显示出来
        startCoroutine()

        // 点击查看更多路线
    }

    // 开启协程获取网络数据
    private fun startCoroutine() = coroutine(Dispatchers.Main) {
        // 获取线路数据
        val result = sApi.getSmartBusAllLIne()

        // 判断是否获取成功
        if (result.code == 200) {
            // 每次重新获取数据时先清除原先数据
            allLineData.clear()

            // 遍历数据存储到数组中
            for (i in result.rows!!) {
                // 创建一个SmartBusAllLineModel对象，并将数据添加到数组中
                allLineData.add(
                    SmartBusAllLineModel(
                        i.id!!,            // 路线ID
                        i.name.toString(),         // 线路名称
                        i.mileage.toString(),      // 线路里程
                        i.first.toString(),        // 起点站
                        i.end.toString(),          // 终点站
                        i.startTime.toString(),    // 首发时间
                        i.endTime.toString()       // 末班时间
                    )
                )
            }

            // 配置列表适配器
            mBinding.smbRecyclerView.apply {
                this.isNestedScrollingEnabled = false  // 禁止嵌套滚动
                this.layoutManager = LinearLayoutManager(context) // 设置布局管理器为线性布局
                this.adapter = SmartBusAllLineRvAdapter(allLineData, R.layout.r_smbs_home_item) // 设置适配器
                this.adapter?.notifyItemChanged(allLineData.size) // 通知适配器数据已改变
            }

        } else runOnUiThread {
            result.msg?.show()
        }
    }

    /**
     * 当点击回退按钮是销毁该界面
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}