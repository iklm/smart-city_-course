package com.example.smartcity.ui.home.News

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smartcity.R
import com.example.smartcity.databinding.ActivityShowNewsListBinding
import com.example.smartcity.logic.Repository.api
import com.example.smartcity.logic.Repository.coroutine
import com.example.smartcity.logic.model.HotNewsModel
import com.example.smartcity.logic.util.Util.show
import com.example.smartcity.ui.home.adapter.HomeNewsRecyclerAdapter

/**
 * 主页搜索新闻结果
 */
// 指定该 Activity 继承自 AppCompatActivity 类
class ShowNewsListActivity : AppCompatActivity() {

    // 定义视图绑定对象
    lateinit var mBinding: ActivityShowNewsListBinding

    // 在 onCreate 方法中进行 Activity 的初始化操作
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // 显示返回按钮
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        // 使用视图绑定库来绑定布局文件，并设置 Activity 的布局
        mBinding = ActivityShowNewsListBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        // 设置标题
        title = "搜索结果"


        // 通过 intent 获取传递过来的内容（标题）
        val content = intent.getStringExtra("content")


        // 当标题不为空时，开启一个协程去获取相关新闻列表
        content?.let {
            coroutine {
                getSeekNews(content)
            }
        }
    }


    /**
     * 获取相关新闻列表
     */
    var arraySeekNews = ArrayList<HotNewsModel.RowsDTO>()
    private suspend fun getSeekNews(title: String){
        // 调用 API 接口获取搜索结果
        var result = api.getSeek(title)
        if (result.code == 200){
            // 清空之前的搜索结果并将当前搜索结果添加到数组中
            arraySeekNews.clear()
            arraySeekNews.addAll(result.rows!!)

            // 将搜索结果展示到 RecyclerView 中
            runOnUiThread {
                mBinding.showNewsListRecyclerView.apply {
                    this.layoutManager = LinearLayoutManager(context)
                    this.isNestedScrollingEnabled = false
                    this.adapter = HomeNewsRecyclerAdapter(arraySeekNews, R.layout.re_show_news)
                    this.adapter?.notifyItemChanged(arraySeekNews.size)
                }
            }
        }else {
            // 若搜索结果返回错误，则展示出错提示
            runOnUiThread {
                "搜索结果: 出错！".show()
            }
        }
    }


    /**
     * 当点击该也返回按钮时，销毁该界面
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // 销毁
        finish()
        return super.onOptionsItemSelected(item)
    }
}