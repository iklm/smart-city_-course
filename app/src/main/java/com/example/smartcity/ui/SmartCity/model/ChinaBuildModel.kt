package com.example.smartcity.ui.SmartCity.model

/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/9/6 16:09
 *  created: Ikun
 *  Email: 2506789532@qq.com
 **/
data class ChinaBuildModel (
    // 中国智造四小模块实体类
    val img: Int,
    val title: String
)