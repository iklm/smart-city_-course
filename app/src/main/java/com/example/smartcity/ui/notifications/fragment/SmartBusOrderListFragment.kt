package com.example.smartcity.ui.notifications.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smartcity.App.Companion.getToken
import com.example.smartcity.R
import com.example.smartcity.databinding.FragmentSmartBusOrderListBinding
import com.example.smartcity.logic.Repository.coroutine
import com.example.smartcity.logic.Repository.sApi
import com.example.smartcity.logic.util.Util.show
import com.example.smartcity.ui.notifications.adapter.SmartBusOrderListAdapter
import com.example.smartcity.ui.notifications.model.SmartBusOrderListModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.internal.notify

/**
 * author: Ikun
 * 智慧巴士订单列表 -  个人中心订单列表展示
 */
class SmartBusOrderListFragment : Fragment() {

    companion object {
        // 订单状态
        var IfOrderNumber = 0
    }

    private lateinit var mBinding: FragmentSmartBusOrderListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        mBinding = FragmentSmartBusOrderListBinding.inflate(inflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getOrderListData(IfOrderNumber)
    }


    /**
     *  获取订单列表数据
     *
     */
    // 存储列表数据
    var orderListData = ArrayList<SmartBusOrderListModel>()
    private fun getOrderListData(status: Int) = coroutine {
        // 先清除数据
        orderListData.clear()
        // 获取订单数据
        var result = sApi.getBusOrderList(getToken, status)

        // 判断是否获取成功
        if (result.code == 200) {
            // 订单状态
            var isStatus = false

            // 将数据添加到数组中
            for (i in result.rows!!) {
                // 判断订单状态
                isStatus = i.status != 0

                orderListData.add(SmartBusOrderListModel(
                    i.orderNum.toString(),
                    isStatus,
                    i.path.toString(),
                    "${i.start} ---> ${i.end}",
                    i.price?.toLong()!!,
                    i.userName.toString(),
                    i.userTel.toString(),
                    i.payTime.toString()
                ))
            }

            activity?.runOnUiThread {
                // 数据添加完成后，我们直接在这里配置适配器吧。
                mBinding.smbsOrderRv.apply {
                    this.isNestedScrollingEnabled = false
                    this.layoutManager = LinearLayoutManager(context)
                    this.adapter = SmartBusOrderListAdapter(orderListData, R.layout.re_person_smbs_orderlist)
                    this.adapter?.notifyItemChanged(orderListData.size)
                }
            }
        } else activity?.runOnUiThread {
            result.msg?.show()
        }
    }

}