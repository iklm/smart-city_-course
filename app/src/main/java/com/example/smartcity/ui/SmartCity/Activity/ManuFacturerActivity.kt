package com.example.smartcity.ui.SmartCity.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.smartcity.R
import com.example.smartcity.databinding.ActivityManuFacturerBinding
import com.example.smartcity.ui.SmartCity.fragment.mOneFragment
import com.example.smartcity.ui.SmartCity.fragment.mTwoFragment
import com.google.android.material.tabs.TabLayoutMediator


/**
 * 中国智造主页-厂商招聘模块
 */
class ManuFacturerActivity : AppCompatActivity() {

    lateinit var mBinding: ActivityManuFacturerBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityManuFacturerBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        // 显示回退按钮
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        // 标题
        title = "厂商招聘"


        // 配置TabLayout/ViewPager2
        settingsTabLayoutOnViewPager2()
    }

    /**
     *  配置TabLayout/ViewPager2
     */
    private fun settingsTabLayoutOnViewPager2() {

        // ViewPager2
        mBinding.mViewPager2.adapter = object : FragmentStateAdapter(this) {

            // 有几个界面
            override fun getItemCount(): Int = 2

            // 根据Tab标签点击返回的Position显示不同的界面
            override fun createFragment(position: Int): Fragment = when(position) {
                0 -> mOneFragment()
                1 -> mTwoFragment()
                else -> mOneFragment()
            }
        }


        // 配置TabLayout
        TabLayoutMediator(mBinding.mTabLayout, mBinding.mViewPager2) { tab, position ->
            when(position) {
                0 -> tab.text = "厂商招聘"
                1 -> tab.text = "校企合作"
                else -> tab.text = "厂商招聘"
            }
        }.attach()
    }

    /**
     * 当点击回退按钮时销毁该界面
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        //
        finish()
        return super.onOptionsItemSelected(item)
    }
}