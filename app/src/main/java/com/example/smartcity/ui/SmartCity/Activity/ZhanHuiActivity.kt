package com.example.smartcity.ui.SmartCity.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.GridLayoutManager
import com.example.smartcity.R
import com.example.smartcity.databinding.ActivityZhanHuiBinding
import com.example.smartcity.ui.SmartCity.adapter.ZhanHuiRvAdapter
import com.example.smartcity.ui.SmartCity.model.ZhanHuiRvModel

/**
 * 中国智造主页-展会活动模块
 */
class ZhanHuiActivity : AppCompatActivity() {

    companion object {
        // 图片/文字/视频这些我就不去找了， 我直接使用之前存储的图片就行了， 你们自己下去找找， 尽量弄的完美一点。
        val ZhanHuiListData = ArrayList<ZhanHuiRvModel>().apply {
            this.add(ZhanHuiRvModel(R.mipmap.a1, "2023年9月14最新活动展开了".repeat(10)))
            this.add(ZhanHuiRvModel(R.mipmap.a2, "2023年9月14最新活动展开了".repeat(10)))
            this.add(ZhanHuiRvModel(R.mipmap.a3, "2023年9月14最新活动展开了".repeat(10)))
            this.add(ZhanHuiRvModel(R.mipmap.a1, "2023年9月14最新活动展开了".repeat(10)))
            this.add(ZhanHuiRvModel(R.mipmap.a2, "2023年9月14最新活动展开了".repeat(10)))
            this.add(ZhanHuiRvModel(R.mipmap.a3, "2023年9月14最新活动展开了".repeat(10)))
            this.add(ZhanHuiRvModel(R.mipmap.a1, "2023年9月14最新活动展开了".repeat(10)))
            this.add(ZhanHuiRvModel(R.mipmap.a2, "2023年9月14最新活动展开了".repeat(10)))
        }
    }

    lateinit var mBinding: ActivityZhanHuiBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityZhanHuiBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        // 显示回退按钮
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        // 设置标题
        title = "展会活动"

        // 列表适配器
        zhanHuiRvAdapterShow()
    }


    /**
     * 列表适配器
     */
    private fun zhanHuiRvAdapterShow() {

        // 最新展会活动
        mBinding.zNewRecyclerView.apply {
            this.isNestedScrollingEnabled = true
            // 我们使用网格布局， 没行显示2个
            this.layoutManager = GridLayoutManager(context, 2)
            // 将我们刚创建好的数据传给适配器， 还有布局
            this.adapter = ZhanHuiRvAdapter(ZhanHuiListData, R.layout.re_zhanhui_item)
            this.adapter?.notifyItemChanged(ZhanHuiListData.size)
        }

        // 过往展会活动
        mBinding.zOldRecyclerView.apply {
            this.isNestedScrollingEnabled = true
            // 我们使用网格布局， 没行显示2个
            this.layoutManager = GridLayoutManager(context, 2)
            // 将我们刚创建好的数据传给适配器， 还有布局
            this.adapter = ZhanHuiRvAdapter(ZhanHuiListData, R.layout.re_zhanhui_item)
            this.adapter?.notifyItemChanged(ZhanHuiListData.size)
        }
    }


    /**
     * 点击回退按钮时销毁该意图（Activity）
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}