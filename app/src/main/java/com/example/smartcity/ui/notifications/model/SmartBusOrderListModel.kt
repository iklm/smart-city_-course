package com.example.smartcity.ui.notifications.model

/**
 *  Android Studio - version 2022.3
 *  createCodeTime: 2023/10/6 13:37
 *  created: Ikun
 *  Email: 2506789532@qq.com
 *
 *  智慧巴士订单列表 - 数据实体类
 *  @see res/layout/re_person_smbs_orderlist.xml
 **/
data class SmartBusOrderListModel(
    val smsOrderNum: String, // 订单号
    val smsStatus: Boolean, // 订单状态
    val smsLineName: String, // 路线名称
    val smsStartAndEnd: String, // 起点到终点
    val smsPrice: Long, // 价格
    val smsUserName: String, // 用户姓名
    val smsUserTel: String, // 用户联系电话
    val smsPayTime: String // 支付时间
)